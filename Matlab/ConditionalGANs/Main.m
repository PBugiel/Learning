%%% https://uk.mathworks.com/help/deeplearning/ug/train-conditional-generative-adversarial-network.html
%% Download and extract the Flowers data set 
url = 'http://download.tensorflow.org/example_images/flower_photos.tgz';
downloadFolder = tempdir;
filename = fullfile(downloadFolder,'flower_dataset.tgz');

imageFolder = fullfile(downloadFolder,'flower_photos');
if ~exist(imageFolder,'dir')
    disp('Downloading Flowers data set (218 MB)...')
    websave(filename,url);
    untar(filename,downloadFolder)
end

% Create an image datastore containing the photos
datasetFolder = fullfile(imageFolder);

imds = imageDatastore(datasetFolder, ...
    'IncludeSubfolders',true, ...
    'LabelSource','foldernames');

% View the number of classes.
classes = categories(imds.Labels);
numClasses = numel(classes)

% Augment the data to include random horizontal flipping and resize the images to have size 64-by-64
augmenter = imageDataAugmenter('RandXReflection',true);
augimds = augmentedImageDatastore([64 64],imds,'DataAugmentation',augmenter);

%% Define Generator Network
% This network:
%   Converts the 1-by-1-by-100 arrays of noise to 4-by-4-by-1024 arrays.
%   Converts the categorical labels to embedding vectors and reshapes them to a 4-by-4 array.
%   Concatenates the resulting images from the two inputs along the channel dimension. The output is a 4-by-1025 array.
%   Upscales the resulting arrays to 64-by-64-by-3 arrays using a series of transposed convolution layers with batch normalization and ReLU layers.
% Define this network architecture as a layer graph and specify the following network properties.
%   For the categorical inputs, use an embedding dimension of 50.
%   For the transposed convolution layers, specify 5-by-5 filters with a decreasing number of filters for each layer, a stride of 2, and "same" cropping of the output.
%   For the final transposed convolution layer, specify a three 5-by-5 filter corresponding to the three RGB channel of the generated images.
% To project and reshape the noise input, use the custom layer projectAndReshapeLayer, attached to this 
% example as a supporting file. The projectAndReshapeLayer object upscales the input using a fully connected
% operation and reshapes the output to the specified size.
% To input the labels into the network, use an imageInputLayer object and specify an image size of 1-by-1. 
% To embed and reshape the label input, use the custom layer embedAndReshapeLayer, attached to this example 
% as a supporting file. The embedAndReshapeLayer object converts a categorical label to a one-channel image of the 
% specified size using an embedding and a fully connected operation.%   At the end of the network, include a tanh layer.

numLatentInputs = 100;
embeddingDimension = 50;
numFilters = 64;

filterSize = 5;
projectionSize = [4 4 1024];

layersGenerator = [
    imageInputLayer([1 1 numLatentInputs],'Normalization','none','Name','noise')
    projectAndReshapeLayer(projectionSize,numLatentInputs,'proj');
    concatenationLayer(3,2,'Name','cat');
    transposedConv2dLayer(filterSize,4*numFilters,'Name','tconv1')
    batchNormalizationLayer('Name','bn1')
    reluLayer('Name','relu1')
    transposedConv2dLayer(filterSize,2*numFilters,'Stride',2,'Cropping','same','Name','tconv2')
    batchNormalizationLayer('Name','bn2')
    reluLayer('Name','relu2')
    transposedConv2dLayer(filterSize,numFilters,'Stride',2,'Cropping','same','Name','tconv3')
    batchNormalizationLayer('Name','bn3')
    reluLayer('Name','relu3')
    transposedConv2dLayer(filterSize,3,'Stride',2,'Cropping','same','Name','tconv4')
    tanhLayer('Name','tanh')];

lgraphGenerator = layerGraph(layersGenerator);

layers = [
    imageInputLayer([1 1],'Name','labels','Normalization','none')
    embedAndReshapeLayer(projectionSize(1:2),embeddingDimension,numClasses,'emb')];

lgraphGenerator = addLayers(lgraphGenerator,layers);
lgraphGenerator = connectLayers(lgraphGenerator,'emb','cat/in2');

% To train the network with a custom training loop and enable automatic differentiation, convert the layer graph to a dlnetwork object.
dlnetGenerator = dlnetwork(lgraphGenerator)




