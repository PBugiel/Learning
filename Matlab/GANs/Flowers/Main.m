%%% https://uk.mathworks.com/help/deeplearning/ug/train-generative-adversarial-network.html#TrainGenerativeAdversarialNetworkGANExample-2
%% Load training data
%url = 'http://download.tensorflow.org/example_images/flower_photos.tgz';
%downloadFolder = tempdir;
%filename = fullfile(downloadFolder,'flower_dataset.tgz');

%imageFolder = fullfile(downloadFolder,'flower_photos');
%if ~exist(imageFolder,'dir')
%    disp('Downloading Flowers data set (218 MB)...')
%    websave(filename,url);
%    untar(filename,downloadFolder)
%end

% add projectAndReshapeLayer.m script directory to search path
addpath('C:\Program Files\MATLAB\R2020a\examples\nnet\main');

% Create an image datastore containing the photos of the flowers/cats/cars.
% https://ai.stanford.edu/~jkrause/cars/car_dataset.html
datasetFolder = fullfile('D:\PhD\CarsDataset\images');
imds = imageDatastore(datasetFolder, 'IncludeSubfolders',true);

%Augment the data to include random horizontal flipping and resize the images to have size 64-by-64.
augmenter = imageDataAugmenter('RandXReflection',true);
augimds = augmentedImageDatastore([128 128],imds,'DataAugmentation',augmenter, 'ColorPreprocessing', 'gray2rgb');

%% Define Generator Network
filterSize = 5;
numFilters = 128;
numLatentInputs = 100;

projectionSize = [4 4 512];

layersGenerator = [
    imageInputLayer([1 1 numLatentInputs],'Normalization','none','Name','in')
    projectAndReshapeLayer(projectionSize,numLatentInputs,'proj');
    transposedConv2dLayer(filterSize,4*numFilters,'Name','tconv1')
    batchNormalizationLayer('Name','bnorm1')
    reluLayer('Name','relu1')
    transposedConv2dLayer(filterSize,2*numFilters,'Stride',2,'Cropping','same','Name','tconv2')
    batchNormalizationLayer('Name','bnorm2')
    reluLayer('Name','relu2')
    transposedConv2dLayer(filterSize,numFilters,'Stride',2,'Cropping','same','Name','tconv3')
    batchNormalizationLayer('Name','bnorm3')
    reluLayer('Name','relu3')
    transposedConv2dLayer(filterSize,numFilters,'Stride',2,'Cropping','same','Name','tconv4')
    batchNormalizationLayer('Name','bnorm4')
    reluLayer('Name','relu4')
    transposedConv2dLayer(filterSize,3,'Stride',2,'Cropping','same','Name','tconv5')
    tanhLayer('Name','tanh')];

lgraphGenerator = layerGraph(layersGenerator);
dlnetGenerator = dlnetwork(lgraphGenerator);

%% Define Discriminator Network
dropoutProb = 0.5;
numFilters = 128;
scale = 0.2;

inputSize = [128 128 3];
filterSize = 5;

layersDiscriminator = [
    imageInputLayer(inputSize,'Normalization','none','Name','in')
    dropoutLayer(0.5,'Name','dropout')
    convolution2dLayer(filterSize,numFilters,'Stride',2,'Padding','same','Name','conv1')
    leakyReluLayer(scale,'Name','lrelu1')
    convolution2dLayer(filterSize,2*numFilters,'Stride',2,'Padding','same','Name','conv2')
    batchNormalizationLayer('Name','bn2')
    leakyReluLayer(scale,'Name','lrelu2')
    convolution2dLayer(filterSize,4*numFilters,'Stride',2,'Padding','same','Name','conv3')
    batchNormalizationLayer('Name','bn3')
    leakyReluLayer(scale,'Name','lrelu3')
    convolution2dLayer(filterSize,8*numFilters,'Stride',2,'Padding','same','Name','conv4')
    batchNormalizationLayer('Name','bn4')
    leakyReluLayer(scale,'Name','lrelu4')
    convolution2dLayer(filterSize,8*numFilters,'Stride',2,'Padding','same','Name','conv5')
    batchNormalizationLayer('Name','bn5')
    leakyReluLayer(scale,'Name','lrelu5')
    convolution2dLayer(4,1,'Name','conv6')];

lgraphDiscriminator = layerGraph(layersDiscriminator);
dlnetDiscriminator = dlnetwork(lgraphDiscriminator);

%% Specify Training Options
numEpochs = 1500;
miniBatchSize = 64;
augimds.MiniBatchSize = miniBatchSize;

% Specify the options for Adam optimization
learnRate = 0.0002;
gradientDecayFactor = 0.5;
squaredGradientDecayFactor = 0.999;

% Train on a GPU if one is available
executionEnvironment = "auto";
flipFactor = 0.3;

% Display the generated validation images every 100 iterations
validationFrequency = 10;

%% Train Model
trailingAvgGenerator = [];
trailingAvgSqGenerator = [];
trailingAvgDiscriminator = [];
trailingAvgSqDiscriminator = [];

% Create an array of held-out random values.
numValidationImages = 25;
ZValidation = randn(1,1,numLatentInputs,numValidationImages,'single');

% Convert the data to dlarray objects and specify the dimension labels 'SSCB' (spatial, spatial, channel, batch).
dlZValidation = dlarray(ZValidation,'SSCB');

% For GPU training, convert the data to gpuArray objects.
if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
    dlZValidation = gpuArray(dlZValidation);
end

% Initialize the training progress plots. Create a figure and resize it to have twice the width.
f = figure;
f.Position(3) = 2*f.Position(3);

% Create a subplot for the generated images and the newtork scores.
imageAxes = subplot(1,2,1);
scoreAxes = subplot(1,2,2);

% Initialize the animated lines for the scores plot.
lineScoreGenerator = animatedline(scoreAxes,'Color',[0 0.447 0.741]);
lineScoreDiscriminator = animatedline(scoreAxes, 'Color', [0.85 0.325 0.098]);
legend('Generator','Discriminator');
ylim([0 1])
xlabel("Iteration")
ylabel("Score")
grid on

% Train the GAN. For each epoch, shuffle the datastore and loop over mini-batches of data.
% For each mini-batch:
% Rescale the images in the range [-1 1].
% Convert the data to dlarray objects with underlying type single and specify the dimension labels 'SSCB' (spatial, spatial, channel, batch).
% Generate a dlarray object containing an array of random values for the generator network.
% For GPU training, convert the data to gpuArray objects.
% Evaluate the model gradients using dlfeval and the modelGradients function.
% Update the network parameters using the adamupdate function.
% Plot the scores of the two networks.
% After every validationFrequency iterations, display a batch of generated images for a fixed held-out generator input.
% Training can take some time to run.
iteration = 0;
start = tic;

% Loop over epochs.
for epoch = 1:numEpochs
    
    % Reset and shuffle datastore.
    reset(augimds);
    augimds = shuffle(augimds);
    
    % Loop over mini-batches.
    while hasdata(augimds)
        iteration = iteration + 1;
        
        % Read mini-batch of data.
        data = read(augimds);
        
        % Ignore last partial mini-batch of epoch.
        if size(data,1) < miniBatchSize
            continue
        end
        
        % Concatenate mini-batch of data and generate latent inputs for the
        % generator network.
        X = cat(4,data{:,1}{:});
        X = single(X);
        Z = randn(1,1,numLatentInputs,size(X,4),'single');
        
        % Rescale the images in the range [-1 1].
        X = rescale(X,-1,1,'InputMin',0,'InputMax',255);
        
        % Convert mini-batch of data to dlarray and specify the dimension labels
        % 'SSCB' (spatial, spatial, channel, batch).
        dlX = dlarray(X, 'SSCB');
        dlZ = dlarray(Z, 'SSCB');
        
        % If training on a GPU, then convert data to gpuArray.
        if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
            dlX = gpuArray(dlX);
            dlZ = gpuArray(dlZ);
        end
        
        % Evaluate the model gradients and the generator state using
        % dlfeval and the modelGradients function listed at the end of the
        % example.
        [gradientsGenerator, gradientsDiscriminator, stateGenerator, scoreGenerator, scoreDiscriminator] = ...
            dlfeval(@modelGradients, dlnetGenerator, dlnetDiscriminator, dlX, dlZ, flipFactor);
        dlnetGenerator.State = stateGenerator;
        
        % Update the discriminator network parameters.
        [dlnetDiscriminator,trailingAvgDiscriminator,trailingAvgSqDiscriminator] = ...
            adamupdate(dlnetDiscriminator, gradientsDiscriminator, ...
            trailingAvgDiscriminator, trailingAvgSqDiscriminator, iteration, ...
            learnRate, gradientDecayFactor, squaredGradientDecayFactor);
        
        % Update the generator network parameters.
        [dlnetGenerator,trailingAvgGenerator,trailingAvgSqGenerator] = ...
            adamupdate(dlnetGenerator, gradientsGenerator, ...
            trailingAvgGenerator, trailingAvgSqGenerator, iteration, ...
            learnRate, gradientDecayFactor, squaredGradientDecayFactor);
        
        % Every validationFrequency iterations, display batch of generated images using the
        % held-out generator input
        if mod(iteration,validationFrequency) == 0 || iteration == 1
            % Generate images using the held-out generator input.
            dlXGeneratedValidation = predict(dlnetGenerator,dlZValidation);
            
            % Tile and rescale the images in the range [0 1].
            I = imtile(extractdata(dlXGeneratedValidation));
            I = rescale(I);
            
            % Display the images.
            subplot(1,2,1);
            image(imageAxes,I)
            xticklabels([]);
            yticklabels([]);
            title("Generated Images");
        end
        
        % Update the scores plot
        subplot(1,2,2)
        addpoints(lineScoreGenerator,iteration,...
            double(gather(extractdata(scoreGenerator))));
        
        addpoints(lineScoreDiscriminator,iteration,...
            double(gather(extractdata(scoreDiscriminator))));
        
        % Update the title with training progress information.
        D = duration(0,0,toc(start),'Format','hh:mm:ss');
        title(...
            "Epoch: " + epoch + ", " + ...
            "Iteration: " + iteration + ", " + ...
            "Elapsed: " + string(D))
        
        drawnow
    end
    if rem(epoch,100) == 0
        save('CarGenerator_' + string(epoch), 'dlnetGenerator');
    end
end

%% Save the generator as mat file
save('CarGenerator', 'dlnetGenerator');

%% Generate New Images
ZNew = randn(1,1,numLatentInputs,25,'single');
dlZNew = dlarray(ZNew,'SSCB');

% To generate images using the GPU, also convert the data to gpuArray objects.
if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
    dlZNew = gpuArray(dlZNew);
end

% Generate new images using the predict function with the generator and the input data.
dlXGeneratedNew = predict(dlnetGenerator,dlZNew);

% Display the images.
I = imtile(extractdata(dlXGeneratedNew));
I = rescale(I);
figure
image(I)
axis off
title("Generated Images")
