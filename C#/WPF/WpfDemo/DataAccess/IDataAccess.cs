﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfDemo.Models;

namespace WpfDemo.DataAccess
{
    public interface IDataAccess
    {
        void WateringSystem_InsertOne(WateringSystem model);
        List<WateringSystem> WateringSystem_GetAll();

        List<PlantContainer> Container_GetAll();

        List<PlantPosition> PlantPosition_GetAll();

        List<PlantType> PlantType_GetAll();


        List<Plant> Plants_GetAll();
    }
}
