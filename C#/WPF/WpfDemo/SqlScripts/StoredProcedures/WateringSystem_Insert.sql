CREATE PROCEDURE dbo.spWateringSystem_Insert 
	@Name nvarchar(100),
	@Id int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.WateringSystem (Name)
	VALUES (@Name);

	SELECT @Id = SCOPE_IDENTITY();
END
GO
