﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDemo.Models
{
    public class PlantContainer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Capacity { get; set; }
    }
}
