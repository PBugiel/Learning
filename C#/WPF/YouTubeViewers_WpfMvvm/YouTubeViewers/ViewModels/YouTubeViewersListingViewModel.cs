﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeViewers.Models;
using YouTubeViewers.Stores;

namespace YouTubeViewers.ViewModels
{
    internal class YouTubeViewersListingViewModel : ViewModelBase
    {
		private readonly ObservableCollection<YouTubeViewersListingItemViewModel> _listingItemViewModels;
        private readonly SelectedYouTubeViewerStore _selectedViewerStore;

        public IEnumerable<YouTubeViewersListingItemViewModel> ListingItemViewModels => _listingItemViewModels;

        private YouTubeViewersListingItemViewModel _selectedListingItemViewModel;
        public YouTubeViewersListingItemViewModel SelectedListingItemViewModel
        {
            get { return _selectedListingItemViewModel; }
            set
            {
                _selectedListingItemViewModel = value;
                OnPropertyChanged(nameof(SelectedListingItemViewModel));
                _selectedViewerStore.SelectedViewer = _selectedListingItemViewModel?.YouTubeViewer;
            }
        }


        public YouTubeViewersListingViewModel(SelectedYouTubeViewerStore selectedViewerStore)
        {
            _selectedViewerStore = selectedViewerStore;
            _listingItemViewModels = new ObservableCollection<YouTubeViewersListingItemViewModel>();
            _listingItemViewModels.Add(new YouTubeViewersListingItemViewModel(new YouTubeViewer("NemesisIrae", false, false)));
            _listingItemViewModels.Add(new YouTubeViewersListingItemViewModel(new YouTubeViewer("LestatDeLincourt", true, false)));
            _listingItemViewModels.Add(new YouTubeViewersListingItemViewModel(new YouTubeViewer("PeterParker", true, true)));
        }
    }
}
