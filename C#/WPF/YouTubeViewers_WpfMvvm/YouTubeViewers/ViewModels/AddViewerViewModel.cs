﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeViewers.ViewModels
{
    public class AddViewerViewModel : ViewModelBase
    {
        public ViewerDetailsFormViewModel ViewerDetailsFormViewModel { get; private set; }
        public AddViewerViewModel()
        {
            ViewerDetailsFormViewModel = new ViewerDetailsFormViewModel();
        }
    }
}
