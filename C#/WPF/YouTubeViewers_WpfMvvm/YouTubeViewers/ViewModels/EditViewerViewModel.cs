﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeViewers.ViewModels
{
    public class EditViewerViewModel
    {
        public ViewerDetailsFormViewModel ViewerDetailsFormViewModel { get; private set; }
        public EditViewerViewModel()
        {
            ViewerDetailsFormViewModel = new ViewerDetailsFormViewModel();
        }
    }
}
