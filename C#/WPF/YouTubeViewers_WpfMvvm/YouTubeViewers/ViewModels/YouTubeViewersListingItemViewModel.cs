﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using YouTubeViewers.Models;

namespace YouTubeViewers.ViewModels
{
    internal class YouTubeViewersListingItemViewModel : ViewModelBase
    {
        public YouTubeViewer YouTubeViewer;

        public string Username => YouTubeViewer.Username;
        public ICommand EditCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public YouTubeViewersListingItemViewModel(YouTubeViewer youTubeViewer)
        {
            YouTubeViewer = youTubeViewer;
        }
    }
}
