﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace YouTubeViewers.ViewModels
{
    public class ViewerDetailsFormViewModel : ViewModelBase
    {
		private string _username;
        private bool _isMember;
        private bool _isSubscribed;

		public string Username
		{
			get { return _username; }
			set
			{
				_username = value;
                OnPropertyChanged(nameof(Username));
                OnPropertyChanged(nameof(CanSubmit));
            }
		}

        public bool IsMember
        {
            get { return _isMember; }
            set
            {
                _isMember = value;
                OnPropertyChanged(nameof(IsMember));
            }
        }

        public bool IsSubscribed
        {
            get { return _isSubscribed; }
            set
            {
                _isSubscribed = value;
                OnPropertyChanged(nameof(IsSubscribed));
            }
        }
        public bool CanSubmit => !string.IsNullOrEmpty(Username);

        public ICommand SubmitCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }

    }
}
