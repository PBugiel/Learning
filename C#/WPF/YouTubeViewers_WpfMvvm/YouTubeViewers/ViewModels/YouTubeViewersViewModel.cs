﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using YouTubeViewers.Stores;

namespace YouTubeViewers.ViewModels
{
    internal class YouTubeViewersViewModel : ViewModelBase
    {
        public YouTubeViewersListingViewModel ListingViewModel { get; }
        public YouTubeViewersDetailsViewModel DetailsViewModel { get; }
        public ICommand AddYouTubeViewersCommand { get; }

        public YouTubeViewersViewModel(SelectedYouTubeViewerStore selectedViewerStore)
        {
            ListingViewModel = new YouTubeViewersListingViewModel(selectedViewerStore);
            DetailsViewModel = new YouTubeViewersDetailsViewModel(selectedViewerStore);
        }
    }
}
