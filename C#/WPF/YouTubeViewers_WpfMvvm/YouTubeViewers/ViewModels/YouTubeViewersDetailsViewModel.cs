﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeViewers.Models;
using YouTubeViewers.Stores;

namespace YouTubeViewers.ViewModels
{
    public class YouTubeViewersDetailsViewModel : ViewModelBase
    {
        private readonly SelectedYouTubeViewerStore _selectedViewerStore;

        private YouTubeViewer SelectedYouTubeViewer => _selectedViewerStore.SelectedViewer;
        public bool HasSelectedViewer => SelectedYouTubeViewer != null;
        public string? Username => SelectedYouTubeViewer?.Username ?? "Unknown";
        public string IsSubscribedDisplay => (SelectedYouTubeViewer?.IsSubscribed ?? false) ? "Yes" : "No";
        public string IsMemberDisplay => (SelectedYouTubeViewer?.IsMember ?? false) ? "Yes" : "No";

        public YouTubeViewersDetailsViewModel(SelectedYouTubeViewerStore selectedViewerStore)
        {
            _selectedViewerStore = selectedViewerStore;
            // subscribing to a store that lives the whole application life
            _selectedViewerStore.SelectedYouTubeViewerChanged += _selectedViewerStore_SelectedYouTubeViewerChanged;
        }

        protected override void Dispose()
        {
            _selectedViewerStore.SelectedYouTubeViewerChanged -= _selectedViewerStore_SelectedYouTubeViewerChanged;
            base.Dispose();
        }

        private void _selectedViewerStore_SelectedYouTubeViewerChanged()
        {
            OnPropertyChanged(nameof(HasSelectedViewer));
            OnPropertyChanged(nameof(Username));
            OnPropertyChanged(nameof(IsSubscribedDisplay));
            OnPropertyChanged(nameof(IsMemberDisplay));
        }
    }
}
