﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeViewers.Models
{
    public class YouTubeViewer
    {
        public string Username { get; private set; }
        public bool IsSubscribed { get; private set; }
        public bool IsMember { get; private set; }

        public YouTubeViewer(string username, bool isSubscribed, bool isMember)
        {
            Username = username;
            IsSubscribed = isSubscribed;
            IsMember = isMember;
        }
    }
}
