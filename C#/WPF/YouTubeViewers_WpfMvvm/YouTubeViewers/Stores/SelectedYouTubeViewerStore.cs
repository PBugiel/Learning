﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeViewers.Models;

namespace YouTubeViewers.Stores
{
    public class SelectedYouTubeViewerStore
    {
		private YouTubeViewer _selectedViewer;

		public YouTubeViewer SelectedViewer
		{
			get { return _selectedViewer; }
			set 
			{ 
				_selectedViewer = value;
				SelectedYouTubeViewerChanged?.Invoke();
			}
		}

		public event Action? SelectedYouTubeViewerChanged;
	}
}
