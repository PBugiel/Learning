﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    public class MatchupModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Competing teams
        /// </summary>
        public List<MatchupEntryModel> Entries { get; set; } = new List<MatchupEntryModel>();

        /// <summary>
        /// Thw ID from the database that will be used to identify the winner
        /// </summary>
        public int WinnerId { get; set; }

        /// <summary>
        /// Winner of a matchup
        /// </summary>
        public TeamModel Winner { get; set; } // TODO fill WinnerId?

        /// <summary>
        /// Round number
        /// </summary>
        public int MatchupRound { get; set; }

        public string DisplayName
        {
            get
            {
                string ret = "";
                foreach (MatchupEntryModel me in Entries)
                {
                    if (me.TeamCompeting != null)
                    {
                        if (ret.Length == 0)
                            ret = me.TeamCompeting.TeamName;
                        else
                            ret += $" vs. {me.TeamCompeting.TeamName}";
                    }
                    else
                    {
                        ret = "Matchup not yet determined";
                        break;
                    }
                }
                return ret;
            }
        }
    }
}
