﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    public class PrizeModel
    {
        /// <summary>
        /// The unique identifier for the prize
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Place number for which the prize is awarded
        /// </summary>
        public int PlaceNumber { get; set; }

        /// <summary>
        /// Custom name of the place
        /// </summary>
        public string PlaceName { get; set; }

        /// <summary>
        /// Amount of money awarded for winning this prize
        /// </summary>
        public decimal PrizeAmount { get; set; }

        /// <summary>
        /// Prize amount as percentage of the collected initial fees
        /// </summary>
        public double PrizePercentage { get; set; }

        public PrizeModel()
        {

        }

        public PrizeModel(string placeName, string placeNumber, string prizeAmount, string prizePercentage)
        {
            PlaceName = placeName;
            int.TryParse(placeNumber, out int placeNumberValue);
            PlaceNumber = placeNumberValue;
            decimal.TryParse(prizeAmount, out decimal prizeAmountValue);
            PrizeAmount = prizeAmountValue;
            if (double.TryParse(prizePercentage, out double prizePercentageValue))
                PrizePercentage = prizePercentageValue;
            else
                PrizePercentage = 0;

        }
    }
}
