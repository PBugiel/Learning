﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary
{
    public static class TournamentLogic
    {
        public static void CreateRounds(TournamentModel tournament)
        {
            List<TeamModel> randomizedTeams = RandomizeTeamOrder(tournament.EnteredTeams);
            int rounds = FindNumberOfRounds(randomizedTeams.Count);
            int byes = NumberOfByes(rounds, randomizedTeams.Count);
            List<MatchupModel> firstRound = CreateFirstRound(byes, randomizedTeams);
            tournament.Rounds.Add(firstRound);
            CreateOtherRounds(tournament, rounds);
        }

        public static void UpdateTournamentResults(TournamentModel model)
        {
            int startingRound = CheckCurrentRound(model);
            List<MatchupModel> toScore = new List<MatchupModel>();
            foreach (List<MatchupModel> round in model.Rounds)
            {
                foreach (MatchupModel rm in round)
                {
                    if (rm.Winner == null && (rm.Entries.Any(x => x.Score != 0) || rm.Entries.Count == 1))
                    {
                        toScore.Add(rm);
                    }
                }
            }

            MarkWinnersInMatchups(toScore);
            AdvanceWinners(toScore, model);
            toScore.ForEach(x => GlobalConfig.Connection.UpdateMatchup(x));
            int endingRound = CheckCurrentRound(model);

            if (endingRound > startingRound)
            {
                model.AlertUsersToNewRound();
            }
        }

        public static void AlertUsersToNewRound(this TournamentModel model)
        {
            int currentRoundNum = CheckCurrentRound(model);
            List<MatchupModel> currentRound = model.Rounds.Where(x => x.First().MatchupRound == currentRoundNum).First();
            foreach (MatchupModel matchup in currentRound)
            {
                foreach (MatchupEntryModel me in matchup.Entries)
                {
                    foreach (PersonModel p in me.TeamCompeting.TeamMembers)
                    {
                        var otherTeamEntry = matchup.Entries.Where(x => x.TeamCompeting != me.TeamCompeting).FirstOrDefault();
                        AlertPersonToNewRound(p, me.TeamCompeting.TeamName, otherTeamEntry);
                    }
                }
            }
        }

        private static void AlertPersonToNewRound(PersonModel p, string teamName, MatchupEntryModel otherTeamEntry)
        {
            if (p.EmailAddress.Length == 0)  // possibly some other e-mail checks
            {
                return;
            }
            string to = "";
            string subject;
            StringBuilder body = new StringBuilder();

            if (otherTeamEntry != null)
            {
                subject = $"You have a new matchup with {otherTeamEntry.TeamCompeting.TeamName}";
                body.AppendLine("<h1>You have a new matchup</h1>");
                body.AppendLine("<strong>Competitor: </strong>");
                body.Append(otherTeamEntry.TeamCompeting.TeamName);
                body.AppendLine();
                body.AppendLine();
                body.AppendLine("Have a great time!");
                body.AppendLine("~Tournament Tracker");
            }
            else
            {
                subject = "You have a bye week this round";
                body.AppendLine("Enjoy your round off!");
                body.AppendLine("~Tournament Tracker");
            }
            to = p.EmailAddress;
            EmailLogic.SendEmail(to, subject, body.ToString());

        }

        private static int CheckCurrentRound(this TournamentModel model)
        {
            int ret = 1;
            foreach (List<MatchupModel> round in model.Rounds)
            {
                if (round.All(x => x.Winner != null))
                {
                    ret += 1;
                }
                else
                {
                    return ret;
                }
            }
            //Tournament is complete
            CompleteTournament(model);
            return ret - 1; // don't go beyond the last round
        }

        private static void CompleteTournament(TournamentModel model)
        {
            GlobalConfig.Connection.CompleteTournament(model);
            TeamModel winners = model.Rounds.Last().First().Winner;
            TeamModel runnerUp = model.Rounds.Last().First().Entries.Where(x => x.TeamCompeting != winners).First().TeamCompeting;

            decimal winnerPrizeAmount = 0;
            decimal runnerUpPrizeAmount = 0;

            // calculate prize money
            if (model.Prizes.Count > 0)
            {
                decimal totalIncome = model.EnteredTeams.Count * model.EntryFee;
                PrizeModel firstPrize = model.Prizes.Where(x => x.PlaceNumber == 1).FirstOrDefault();
                PrizeModel secondPrize = model.Prizes.Where(x => x.PlaceNumber == 2).FirstOrDefault();
                if (firstPrize != null)
                {
                    winnerPrizeAmount = firstPrize.CalculatePrizePayout(totalIncome);
                }
                if (secondPrize != null)
                {
                    runnerUpPrizeAmount = secondPrize.CalculatePrizePayout(totalIncome);
                }
            }

            // Send Email to all tournaments participants
            string subject;
            StringBuilder body = new StringBuilder();

            subject = $"In {model.TournamentName}, {winners.TeamName} has won!";
            body.AppendLine("<h1>We have a winner!</h1>");
            body.AppendLine("<p>Congratulations to out winner on a great tournament.</p>");
            body.AppendLine("<br />");
            if (winnerPrizeAmount > 0)
            {
                body.AppendLine($"<p>{winners.TeamName} will receive ${winnerPrizeAmount}</p>");
            }
            if (runnerUpPrizeAmount > 0)
            {
                body.AppendLine($"<p>{runnerUp.TeamName} will receive ${runnerUpPrizeAmount}</p>");
            }
            body.AppendLine("<p>Thans for a great tournament everyone!</p>");
            body.AppendLine("~Tournament Tracker");

            List<string> bcc = new List<string>();
            foreach (TeamModel team in model.EnteredTeams)
            {
                foreach (PersonModel person in team.TeamMembers)
                {
                    if (person.EmailAddress.Length > 0)
                    {
                        bcc.Add(person.EmailAddress);
                    }
                }
            }
            EmailLogic.SendEmail(new List<string>(), bcc, subject, body.ToString());

            // Fire the event
            model.CompleteTournament();
        }

        private static decimal CalculatePrizePayout(this PrizeModel prize, decimal totalIncome)
        {
            decimal ret = 0;
            if (prize.PrizeAmount > 0)
            {
                ret = prize.PrizeAmount;
            }
            else
            {
                ret = decimal.Multiply(totalIncome, Convert.ToDecimal(prize.PrizePercentage / 100));
            }
            return ret;
        }

        private static void AdvanceWinners(List<MatchupModel> models, TournamentModel tournament)
        {
            foreach (MatchupModel m in models)
            {
                foreach (List<MatchupModel> round in tournament.Rounds)
                {
                    foreach (MatchupModel rm in round)
                    {
                        foreach (MatchupEntryModel me in rm.Entries)
                        {
                            if (me.ParentMatchup != null && me.ParentMatchup.Id == m.Id)
                            {
                                me.TeamCompeting = m.Winner;
                                GlobalConfig.Connection.UpdateMatchup(m);
                            }
                        }
                    }
                }
            }
        }

        private static void MarkWinnersInMatchups(List<MatchupModel> models)
        {
            // greater or lesser score wins 
            string greaterWins = ConfigurationManager.AppSettings["greaterWins"];

            foreach (MatchupModel m in models)
            {
                if (m.Entries.Count == 1)
                {
                    m.Winner = m.Entries[0].TeamCompeting;
                    continue;
                }
                if (greaterWins == "0")
                {
                    // 0 (false) means that low score wins
                    if (m.Entries[0].Score < m.Entries[1].Score)
                        m.Winner = m.Entries[0].TeamCompeting;
                    else if (m.Entries[1].Score < m.Entries[0].Score)
                        m.Winner = m.Entries[1].TeamCompeting;
                    else
                        throw new Exception("We do not allow ties in this application.");
                }
                else
                {
                    // 1 or anything else means that high score wins
                    if (m.Entries[0].Score > m.Entries[1].Score)
                        m.Winner = m.Entries[0].TeamCompeting;
                    else if (m.Entries[1].Score > m.Entries[0].Score)
                        m.Winner = m.Entries[1].TeamCompeting;
                    else
                        throw new Exception("We do not allow ties in this application.");
                }
            }
        }

        public static void CreateOtherRounds(TournamentModel tournament, int rounds)
        {
            int round = 2;
            List<MatchupModel> prevRound = tournament.Rounds[0];
            List<MatchupModel> currRound = new List<MatchupModel>();
            MatchupModel currMatchup = new MatchupModel();

            while (round <= rounds)
            {
                foreach (MatchupModel match in prevRound)
                {
                    currMatchup.Entries.Add(new MatchupEntryModel { ParentMatchup = match });
                    if (currMatchup.Entries.Count > 1)
                    {
                        currMatchup.MatchupRound = round;
                        currRound.Add(currMatchup);
                        currMatchup = new MatchupModel();
                    }
                }
                tournament.Rounds.Add(currRound);
                round++;
                prevRound = currRound;
                currRound = new List<MatchupModel>();
            }
        }

        private static List<MatchupModel> CreateFirstRound(int byes, List<TeamModel> teams)
        {
            List<MatchupModel> ret = new List<MatchupModel>();
            MatchupModel currentMatchup = new MatchupModel();
            foreach (TeamModel team in teams)
            {
                currentMatchup.Entries.Add(new MatchupEntryModel { TeamCompeting = team });
                if (byes > 0 || currentMatchup.Entries.Count > 1)
                {
                    currentMatchup.MatchupRound = 1;
                    ret.Add(currentMatchup);
                    currentMatchup = new MatchupModel();

                    if (byes > 0) byes -= 1;
                }
            }
            return ret;
        }

        private static int NumberOfByes(int rounds, int teamCount)
        {
            int totalTeams =  (int)Math.Pow(2, rounds);
            return totalTeams - teamCount;
        }
        private static int FindNumberOfRounds(int teamCount)
        {
            int power = 1;
            for (int totalTeams = 2; totalTeams < teamCount; totalTeams*=2)
            {
                power++;
            }
            return power;
        }

        private static List<TeamModel> RandomizeTeamOrder(List<TeamModel> teams)
        {
            // pseudo randomize
            return teams.OrderBy(x => Guid.NewGuid()).ToList();
        }
    }
}
