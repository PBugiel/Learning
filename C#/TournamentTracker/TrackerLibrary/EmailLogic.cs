﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace TrackerLibrary
{
    public static class EmailLogic
    {
        public static void SendEmail(string to, string subject, string body)
        {
            SendEmail(new List<string> { to }, new List<string>(), subject, body); 
        }
        public static void SendEmail(List<string> to, List<string> bcc, string subject, string body)
        {
            string fromAddress = GlobalConfig.AppKeyLookup("senderEmail");
            string fromDisplayName = GlobalConfig.AppKeyLookup("senderDisplayName");
            
            MailAddress fromMailAddress = new MailAddress(fromAddress, fromDisplayName);
            MailMessage mail = new MailMessage();
            foreach (string address in to)
            {
                mail.To.Add(address);
            }
            foreach (string address in bcc)
            {
                mail.Bcc.Add(address);
            }
            mail.From = fromMailAddress;
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();
            client.Send(mail);
        }
    }
}
