﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary.DataAccess
{
    public class SqlConnector : IDataConnection
    {
        private const string db = "Tournaments";
        /// <summary>
        /// Saves a new prize to the database
        /// </summary>
        /// <param name="model">The prize information</param>
        /// <returns>The prize information</returns>
        public void CreatePrize(PrizeModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@PlaceNumber", model.PlaceNumber);
                p.Add("@PlaceName", model.PlaceName);
                p.Add("@PrizeAmount", model.PrizeAmount);
                p.Add("@PrizePercentage", model.PrizePercentage);
                p.Add("@id", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                connection.Execute("dbo.spPrizes_Insert", p, commandType:CommandType.StoredProcedure);
                model.Id = p.Get<int>("@id");
            }
        }

        public void CreatePerson(PersonModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@FirstName", model.FirstName);
                p.Add("@LastName", model.LastName);
                p.Add("@EmailAddress", model.EmailAddress);
                p.Add("@PhoneNumber", model.CellphoneNumber);
                p.Add("@id", 0, DbType.Int32, direction: ParameterDirection.Output);
                connection.Execute("dbo.spPeople_Insert", p, commandType:CommandType.StoredProcedure);
                model.Id = p.Get<int>("@id");
            }
        }

        public List<PersonModel> GetPerson_All()
        {
            List<PersonModel> ret;
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                ret = connection.Query<PersonModel>("dbo.spPeople_GetAll").ToList();
            }
            return ret;
        }

        public void CreateTeam(TeamModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var pTeam = new DynamicParameters();
                pTeam.Add("@TeamName", model.TeamName);
                pTeam.Add("@id", 0, DbType.Int32, ParameterDirection.Output);
                connection.Execute("dbo.spTeams_Insert", pTeam, commandType: CommandType.StoredProcedure);
                model.Id = pTeam.Get<int>("@id");

                foreach (PersonModel tm in model.TeamMembers)
                {
                    var pMember = new DynamicParameters();
                    pMember.Add("@TeamId", model.Id);
                    pMember.Add("@PersonId", tm.Id);
                    connection.Execute("dbo.spTeamMembers_Insert", pMember, commandType: CommandType.StoredProcedure);
                }
            }
        }

        public List<TeamModel> GetTeam_All()
        {
            List<TeamModel> ret;
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                ret = connection.Query<TeamModel>("dbo.spTeams_GetAll").ToList();
                foreach (TeamModel team in ret)
                {
                    var teamId = new DynamicParameters();
                    teamId.Add("@TeamId", team.Id);
                    team.TeamMembers = connection.Query<PersonModel>("dbo.spTeamMembers_GetByTeam", teamId, commandType:CommandType.StoredProcedure).ToList();
                }
            }
            return ret;
        }

        public void CreateTournament(TournamentModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                SaveTournament(model, connection);
                SaveTournamentPrizes(model, connection);
                SaveTournamentEntries(model, connection);
                SaveTournamentRounds(model, connection);
                TournamentLogic.UpdateTournamentResults(model);
            }
        }

        private void SaveTournamentRounds(TournamentModel model, IDbConnection connection)
        {
            // save TournamentModel: List<List<MatchupModel>> Rounds -> one round: List<MatchupModel>
            // save MatchupModel:    List<MatchupEntryModel> Entries -> MatchupEntryModel needs MatchupModel ParentMatchup

            // Loop through the Rounds
            // Loop through the Matchups
            // Loop through the Matchup Entries and save them
            foreach (List<MatchupModel> round in model.Rounds)
            {
                foreach (MatchupModel matchup in round)
                {
                    // Save the Matchup
                    var pMatchup = new DynamicParameters();
                    pMatchup.Add("@TournamentId", model.Id);
                    pMatchup.Add("@MatchupRound", matchup.MatchupRound);
                    pMatchup.Add("@id", 0, DbType.Int32, ParameterDirection.Output);
                    connection.Execute("dbo.spMatchups_Insert", pMatchup, commandType: CommandType.StoredProcedure);
                    matchup.Id = pMatchup.Get<int>("@id");

                    foreach (MatchupEntryModel entry in matchup.Entries)
                    {
                        // Save the Matchup Entry to DB
                        var pEntry = new DynamicParameters();
                        pEntry.Add("@MatchupId", matchup.Id);

                        if (entry.ParentMatchup == null)
                            pEntry.Add("@ParentMatchupId", null);
                        else
                            pEntry.Add("@ParentMatchupId", entry.ParentMatchup.Id);

                        if (entry.TeamCompeting == null)
                            pEntry.Add("@TeamCompetingId", null);
                        else
                            pEntry.Add("@TeamCompetingId", entry.TeamCompeting.Id);

                        pEntry.Add("@id", 0, DbType.Int32, ParameterDirection.Output);
                        connection.Execute("dbo.spMatchupEntries_Insert", pEntry, commandType: CommandType.StoredProcedure);
                        entry.Id = pMatchup.Get<int>("@id");
                    }
                }
            }
        }

        private void SaveTournament(TournamentModel model, IDbConnection connection)
        {
            var pTourn = new DynamicParameters();
            pTourn.Add("@TournamentName", model.TournamentName);
            pTourn.Add("@EntryFee", model.EntryFee);
            pTourn.Add("@id", 0, DbType.Int32, ParameterDirection.Output);
            connection.Execute("dbo.spTournaments_Insert", pTourn, commandType: CommandType.StoredProcedure);
            model.Id = pTourn.Get<int>("@id");
        }

        private void SaveTournamentPrizes(TournamentModel model, IDbConnection connection)
        {
            foreach (PrizeModel pm in model.Prizes)
            {
                var pPrize = new DynamicParameters();
                pPrize.Add("@TournamentId", model.Id);
                pPrize.Add("@PrizeId", pm.Id);
                connection.Execute("dbo.spTournamentPrizes_Insert", pPrize, commandType: CommandType.StoredProcedure);
            }
        }

        private void SaveTournamentEntries(TournamentModel model, IDbConnection connection)
        {
            foreach (TeamModel tm in model.EnteredTeams)
            {
                var pTeam = new DynamicParameters();
                pTeam.Add("@TournamentId", model.Id);
                pTeam.Add("@TeamId", tm.Id);
                connection.Execute("dbo.spTournamentEntries_Insert", pTeam, commandType: CommandType.StoredProcedure);
            }
        }

        public List<TournamentModel> GetTournament_All()
        {
            List<TournamentModel> ret = new List<TournamentModel>();
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                ret = connection.Query<TournamentModel>("dbo.spTournaments_GetAll").ToList();
                foreach (TournamentModel tm in ret)
                {
                    var p = new DynamicParameters();
                    p.Add("@TournamentId", tm.Id);
                    // Fill Prizes
                    tm.Prizes = connection.Query<PrizeModel>("dbo.spPrizes_GetByTournamentId", p, commandType: CommandType.StoredProcedure).ToList();

                    // Fill Teams
                    tm.EnteredTeams = connection.Query<TeamModel>("dbo.spTeam_GetByTournamentId", p, commandType: CommandType.StoredProcedure).ToList();
                    foreach (TeamModel team in tm.EnteredTeams)
                    {
                        var t = new DynamicParameters();
                        t.Add("@TeamId", team.Id);
                        team.TeamMembers = connection.Query<PersonModel>("dbo.spTeamMembers_GetByTeam", t, commandType: CommandType.StoredProcedure).ToList();
                    }

                    // Fill in information on all the Matchups belonging to this Tournament
                    List<MatchupModel> matchups = connection.Query<MatchupModel>("dbo.spMatchups_GetByTournament", p, commandType: CommandType.StoredProcedure).ToList();
                    foreach (MatchupModel m in matchups)
                    {
                        var e = new DynamicParameters();
                        e.Add("@MatchupId", m.Id);
                        m.Entries = connection.Query<MatchupEntryModel>("dbo.spMatchupEntries_GetByMatchup", e, commandType: CommandType.StoredProcedure).ToList();

                        // In MatchupModel, populate Winner based on WinnerId
                        List<TeamModel> allTeams = GetTeam_All();
                        if (m.WinnerId > 0)
                        {
                            m.Winner = allTeams.Where(x => x.Id == m.WinnerId).First();
                        }
                        // In MatchupEntryModel, populate TeamCompeting based on TeamCompetingId
                        //      and ParentMatchup basen on ParentMatchupId
                        foreach (var me in m.Entries)
                        {
                            if (me.TeamCompetingId > 0)
                            {
                                me.TeamCompeting = allTeams.Where(x => x.Id == me.TeamCompetingId).First();
                            }

                            if (me.ParentMatchupId > 0)
                            {
                                me.ParentMatchup = matchups.Where(x => x.Id == me.ParentMatchupId).First();
                            }
                        }
                    }

                    // Fill Rounds
                    List<MatchupModel> currRow = new List<MatchupModel>();
                    int currRound = 1;
                    foreach (MatchupModel m in matchups)
                    {
                        if(m.MatchupRound > currRound)
                        {
                            tm.Rounds.Add(currRow);
                            currRow = new List<MatchupModel>();
                            currRound++;
                        }
                        currRow.Add(m);
                    }
                    tm.Rounds.Add(currRow);
                }
            }
                return ret;
        }

        public void UpdateMatchup(MatchupModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                if (model.Winner != null)
                {
                    p.Add("@id", model.Id);
                    p.Add("@WinnerId", model.Winner.Id);
                    connection.Execute("dbo.spMatchup_Update", p, commandType: CommandType.StoredProcedure);
                }
                foreach (MatchupEntryModel me in model.Entries)
                {
                    if (me.TeamCompeting != null)
                    {
                        p = new DynamicParameters();
                        p.Add("@id", me.Id);
                        p.Add("@TeamCompetingId", me.TeamCompeting.Id);
                        p.Add("@Score", me.Score);
                        connection.Execute("dbo.spMatchupEntries_Update", p, commandType: CommandType.StoredProcedure); 
                    }
                }
            }
        }

        public void CompleteTournament(TournamentModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.ConnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.Id);
                connection.Execute("dbo.spTournaments_Complete", p, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
