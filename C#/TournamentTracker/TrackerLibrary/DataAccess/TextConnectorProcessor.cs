﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary.DataAccess.TextHelpers
{
    public static class TextConnectorProcessor
    {
        public static string FullFilePath(this string fileName)
        {
            return $"{ConfigurationManager.AppSettings["filePath"]}\\{fileName}";
        }

        public static List<string> LoadFile(this string filePath)
        {
            if (!File.Exists(filePath))
            {
                return new List<string>();
            }

            return File.ReadAllLines(filePath).ToList();
        }

        public static List<PrizeModel> ConvertToPrizeModels(this List<string> lines)
        {
            List<PrizeModel> ret = new List<PrizeModel>();
            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                PrizeModel p = new PrizeModel();
                p.Id = int.Parse(cols[0]);
                p.PlaceNumber = int.Parse(cols[1]);
                p.PlaceName = cols[2];
                p.PrizeAmount = decimal.Parse(cols[3]);
                p.PrizePercentage = double.Parse(cols[4]);
                ret.Add(p);
            }
            return ret;
        }

        public static void SaveToPrizeFile(this List<PrizeModel> models)
        {
            List<string> lines = new List<string>();
            foreach (PrizeModel p in models)
            {
                lines.Add($"{p.Id},{p.PlaceNumber},{p.PlaceName},{p.PrizeAmount},{p.PrizePercentage}");
            }
            File.WriteAllLines(GlobalConfig.PrizesFile.FullFilePath(), lines);
        }

        public static List<PersonModel> ConvertToPersonModels(this List<string> lines)
        {
            List<PersonModel> ret = new List<PersonModel>();
            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                PersonModel p = new PersonModel();
                p.Id = int.Parse(cols[0]);
                p.FirstName = cols[1];
                p.LastName = cols[2];
                p.EmailAddress = cols[3];
                p.CellphoneNumber = cols[4];
                ret.Add(p);
            }
            return ret;
        }

        public static void SaveToPeopleFile(this List<PersonModel> models)
        {
            List<string> lines = new List<string>();
            foreach (var p in models)
            {
                lines.Add($"{p.Id},{p.FirstName},{p.LastName},{p.EmailAddress},{p.CellphoneNumber}");
            }
            File.WriteAllLines(GlobalConfig.PeopleFile.FullFilePath(), lines);
        }

        public static List<TeamModel> ConvertToTeamModels(this List<string> lines)
        {
            // id, teamName, listOfIdsSeparatedByPipe
            // 3, Some Team, 2|3|4
            List<TeamModel> ret = new List<TeamModel>();
            List<PersonModel> people = GlobalConfig.PeopleFile.FullFilePath().LoadFile().ConvertToPersonModels();
            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                TeamModel t = new TeamModel();
                t.Id = int.Parse(cols[0]);
                t.TeamName = cols[1];
                string[] memberIds = cols[2].Split('|');
                List<PersonModel> teamMembers = new List<PersonModel>();
                foreach (string memberId in memberIds)
                {
                    if (memberId.Length > 0)
                        t.TeamMembers.Add(people.Where(x => x.Id == int.Parse(memberId)).First());
                }
                ret.Add(t);
            }
            return ret;
        }

        public static void SaveToTeamsFile(this List<TeamModel> models, string teamsFile)
        {
            List<string> lines = new List<string>();
            foreach (var t in models)
            {
                string members = ConvertPeopleListToString(t.TeamMembers);
                lines.Add($"{t.Id},{t.TeamName},{members}");
            }
            File.WriteAllLines(teamsFile.FullFilePath(), lines);
        }

        public static List<TournamentModel> ConvertToTournamentModels(this List<string> lines)
        {
            // id, TournamentName, EntryFee, (id|id|id - enetered teams), (id|id|id - prizes),
            // (Rounds - id^id^id|id^id^id|id^id)
            List<TournamentModel> ret = new List<TournamentModel>();
            List<TeamModel> allTeams = GlobalConfig.TeamsFile.FullFilePath().LoadFile().ConvertToTeamModels();
            List<PrizeModel> allPrizes = GlobalConfig.PrizesFile.FullFilePath().LoadFile().ConvertToPrizeModels();
            List<MatchupModel> allMatchups = GlobalConfig.MatchupsFile.FullFilePath().LoadFile().ConvertToMatchupModels();

            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                TournamentModel tm = new TournamentModel()
                {
                    Id = int.Parse(cols[0]),
                    TournamentName = cols[1],
                    EntryFee = decimal.Parse(cols[2])
                };

                string[] teamIds = cols[3].Split('|');
                foreach (string teamId in teamIds)
                {
                    if (teamId.Length > 0)
                        tm.EnteredTeams.Add(allTeams.Where(x => x.Id == int.Parse(teamId)).First());
                }

                string[] prizeIds = cols[4].Split('|');
                foreach (string prizeId in prizeIds)
                {
                    if (prizeId.Length > 0)
                        tm.Prizes.Add(allPrizes.Where(x => x.Id == int.Parse(prizeId)).First());
                }

                string[] roundIdLists = cols[5].Split('|');
                foreach (string roundIdList in roundIdLists)
                {
                    if(roundIdList.Length > 0)
                    {
                        List<MatchupModel> round = new List<MatchupModel>();
                        string[] roundIds = roundIdList.Split('^');
                        foreach (string matchupId in roundIds)
                        {
                            round.Add(allMatchups.Where(x => x.Id == int.Parse(matchupId)).First());
                        }
                        tm.Rounds.Add(round);
                    }
                }
                ret.Add(tm);
            }
            return ret;
        }

        public static void SaveToTournamentsFile(this List<TournamentModel> tournaments, string fileName)
        {
            List<string> lines = new List<string>();
            foreach (TournamentModel tm in tournaments)
            {
                string teams = ConvertTeamsListToString(tm.EnteredTeams);
                string prizes = ConvertPrizeListToString(tm.Prizes);
                string rounds = ConvertRoundsListToString(tm.Rounds);
                lines.Add($"{tm.Id},{tm.TournamentName},{tm.EntryFee},{teams},{prizes},{rounds}");
            }
            File.WriteAllLines(fileName.FullFilePath(), lines);
        }

        private static string ConvertPeopleListToString(List<PersonModel> people)
        {
            string ret = "";
            foreach (PersonModel person in people)
            {
                ret += $"{person.Id}|";
            }
            ret = ret.Trim('|');
            return ret;
        }

        private static string ConvertTeamsListToString(List<TeamModel> teams)
        {
            string ret = "";
            foreach (TeamModel team in teams)
            {
                ret += $"{team.Id}|";
            }
            ret = ret.Trim('|');
            return ret;
        }

        private static string ConvertPrizeListToString(List<PrizeModel> prizes)
        {
            string ret = "";
            foreach (PrizeModel prize in prizes)
            {
                ret += $"{prize.Id}|";
            }
            ret = ret.Trim('|');
            return ret;
        }

        private static string ConvertRoundsListToString(List<List<MatchupModel>> rounds)
        {
            string ret = "";
            foreach (List<MatchupModel> matchups in rounds)
            {
                ret += $"{ConvertMatchupListToString(matchups)}|";
            }
            ret = ret.Trim('|');
            return ret;
        }

        private static string ConvertMatchupListToString(List<MatchupModel> matchups)
        {
            string ret = "";
            foreach (MatchupModel matchup in matchups)
            {
                ret += $"{matchup.Id}^";
            }
            ret = ret.Trim('^');
            return ret;
        }

        public static void SaveToRoundsFile(this TournamentModel tournament, string matchupFile, string matchupEntryFile)
        {
            // Loop through each round
            // Loop through each matchup
            // Get the ID for the new matchup and save the record
            // Loop through each entry, get the ID and save it
            foreach (List<MatchupModel> round in tournament.Rounds)
            {
                foreach (MatchupModel matchup in round)
                {
                    // Load all matchups to get the top ID
                    // Store the ID
                    // Save the matchup entry
                    matchup.SaveMatchupToFile(matchupFile, matchupEntryFile);
                }
            }
        }

        private static List<MatchupEntryModel> ConvertStringToMatchupEntryModels(string line)
        {
            string[] ids = line.Split('|');
            List<MatchupEntryModel> ret = new List<MatchupEntryModel>();
            List<string> allEntries = GlobalConfig.MatchupEntriesFile.FullFilePath().LoadFile();
            List<string> matchingEntries = new List<string>();
            foreach (string id in ids)
            {
                foreach (string entryLine in allEntries)
                {
                    if (entryLine.Split(',')[0] == id) matchingEntries.Add(entryLine);
                }
            }
            ret = matchingEntries.ConvertToMatchupEntryModels();
            return ret;
        }

        private static TeamModel LookUpTeamById(int id)
        {
            List<string> teams = GlobalConfig.TeamsFile.FullFilePath().LoadFile();
            foreach (string teamLine in teams)
            {
                if (teamLine.Split(',')[0] == id.ToString())
                {
                    List<string> matchingTeam = new List<string>();
                    matchingTeam.Add(teamLine);
                    return matchingTeam.ConvertToTeamModels().First();
                }
            }
            return null;
        }
        private static MatchupModel LookUpMatchupById(int id)
        {
            List<string> matchups = GlobalConfig.MatchupsFile.FullFilePath().LoadFile();
            foreach (string matchupLine in matchups)
            {
                if (matchupLine.Split(',')[0] == id.ToString())
                {
                    List<string> matchingMatchup = new List<string>();
                    matchingMatchup.Add(matchupLine);
                    return matchingMatchup.ConvertToMatchupModels().First();
                }
            }
            return null;
        }

        public static List<MatchupEntryModel> ConvertToMatchupEntryModels(this List<string> lines)
        {
            // id, TeamCompeting, Score, ParentMatchup
            List<MatchupEntryModel> ret = new List<MatchupEntryModel>();
            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                MatchupEntryModel m = new MatchupEntryModel();
                m.Id = int.Parse(cols[0]);
                if (int.TryParse(cols[1], out int teamCompetingId))
                    m.TeamCompeting = LookUpTeamById(teamCompetingId);
                else
                    m.TeamCompeting = null;
                m.Score = double.Parse(cols[2]);
                if (int.TryParse(cols[3], out int parentMatchupId))
                    m.ParentMatchup = LookUpMatchupById(parentMatchupId);
                else
                    m.ParentMatchup = null;
                ret.Add(m);
            }
            return ret;
        }

        public static List<MatchupModel> ConvertToMatchupModels(this List<string> lines)
        {
            // ID,entries(pipe delimited),winner,matchupRound
            List<MatchupModel> ret = new List<MatchupModel>();
            foreach (string line in lines)
            {
                string[] cols = line.Split(',');
                MatchupModel m = new MatchupModel();
                m.Id = int.Parse(cols[0]);
                m.Entries = ConvertStringToMatchupEntryModels(cols[1]);
                if (cols[2] == "")
                    m.Winner = null;
                else
                    m.Winner = LookUpTeamById(int.Parse(cols[2]));
                m.MatchupRound = int.Parse(cols[3]);
                ret.Add(m);
            }
            return ret;
        }

        public static void SaveMatchupToFile(this MatchupModel matchup, string matchupFile, string matchupEntryFile)
        {
            //List<TournamentModel> tournaments = TournamentsFile.FullFilePath().LoadFile().ConvertToTournamentModels(TeamsFile, PeopleFile, PrizesFile);
            List<MatchupModel> matchups = GlobalConfig.MatchupsFile.FullFilePath().LoadFile().ConvertToMatchupModels();
            var lastMatchup = matchups.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = lastMatchup == null ? 1 : lastMatchup.Id + 1;
            matchup.Id = currentId;

            foreach (MatchupEntryModel entry in matchup.Entries)
            {
                entry.SaveEntryToFile(matchupEntryFile);
            }
            matchups.Add(matchup);

            // Save to file
            List<string> lines = new List<string>();
            foreach (MatchupModel m in matchups)
            {
                // ID,entries(pipe delimited),winner,matchupRound
                string entries = ConvertMatchupEntryListToString(m.Entries);
                string winnerId = "";
                if (m.Winner != null)
                    winnerId = m.Winner.Id.ToString();
                lines.Add($"{m.Id},{entries},{winnerId},{m.MatchupRound}");
            }
            File.WriteAllLines(GlobalConfig.MatchupsFile.FullFilePath(), lines);
        }

        private static string ConvertMatchupToString(MatchupModel model)
        {
            string ret = "";
            string entries = ConvertMatchupEntryListToString(model.Entries);
            string winnerId = "";
            if (model.Winner != null)
                winnerId = model.Winner.Id.ToString();
            ret = $"{model.Id},{entries},{winnerId},{model.MatchupRound}";
            return ret;
        }

        private static string ConvertMatchupEntryListToString(List<MatchupEntryModel> entries)
        {
            string ret = "";
            foreach (MatchupEntryModel e in entries)
            {
                ret += $"{e.Id}|";
            }
            return ret.Trim('|');
        }

        public static void SaveEntryToFile(this MatchupEntryModel entry, string matchupEntryFile)
        {
            List<MatchupEntryModel> entries = GlobalConfig.MatchupEntriesFile.FullFilePath().LoadFile().ConvertToMatchupEntryModels();
            var lastEntry = entries.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = lastEntry == null ? 1 : lastEntry.Id + 1;
            entry.Id = currentId;
            entries.Add(entry);
            // Save to file
            List<string> lines = new List<string>();
            foreach (var e in entries)
            {
                lines.Add(ConvertMatchupEntryToString(e));
            }
            File.WriteAllLines(GlobalConfig.MatchupEntriesFile.FullFilePath(), lines);
        }

        // TODO Extension Method?
        public static string ConvertMatchupEntryToString(MatchupEntryModel model)
        {
            // id, TeamCompeting, Score, ParentMatchup
            string parentId = "";
            if (model.ParentMatchup != null)
                parentId = model.ParentMatchup.Id.ToString();
            string teamCompetingId = "";
            if (model.TeamCompeting != null)
            {
                teamCompetingId = model.TeamCompeting.Id.ToString();
            }
            return $"{model.Id},{teamCompetingId},{model.Score},{parentId}";
        }

        public static void UpdateMatchupToFile(this MatchupModel matchup)
        {
            List<MatchupModel> allMatchups = GlobalConfig.MatchupsFile.FullFilePath().LoadFile().ConvertToMatchupModels();
            List<string> lines = new List<string>();
            MatchupModel toRemove = allMatchups.Where(x => x.Id == matchup.Id).FirstOrDefault();
            if (toRemove == null)
                return;
                
            allMatchups.Remove(toRemove);
            foreach (MatchupEntryModel e in matchup.Entries)
            {
                e.UpdateEntryToFile();
            }
            allMatchups.Add(matchup);

            foreach (MatchupModel m in allMatchups)
            {
                lines.Add(ConvertMatchupToString(m));
            }
            File.WriteAllLines(GlobalConfig.MatchupsFile.FullFilePath(), lines);
        }

        public static void UpdateEntryToFile(this MatchupEntryModel model)
        {
            List<MatchupEntryModel> allEntries = GlobalConfig.MatchupEntriesFile.FullFilePath().LoadFile().ConvertToMatchupEntryModels();
            List<string> lines = new List<string>();
            MatchupEntryModel toRemove = allEntries.Where(x => x.Id == model.Id).FirstOrDefault();
            if (toRemove == null)
                return;
            allEntries.Remove(toRemove);
            allEntries.Add(model);
            foreach (var e in allEntries)
            {
                lines.Add(ConvertMatchupEntryToString(e));
            }
            File.WriteAllLines(GlobalConfig.MatchupEntriesFile.FullFilePath(), lines);
        }
    }
}
