﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;
using TrackerLibrary.DataAccess.TextHelpers;

namespace TrackerLibrary.DataAccess
{
    public class TextConnector : IDataConnection
    {
        public void CreatePrize(PrizeModel model)
        {
            // Load txt file and convert to List<PrizeModel>
            List<PrizeModel> prizes = GlobalConfig.PrizesFile.FullFilePath().LoadFile().ConvertToPrizeModels();

            // Find last ID
            var prize = prizes.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = prize == null ? 1 : prize.Id + 1;
            model.Id = currentId;

            // Add new record
            prizes.Add(model);

            // Save the List<string> to the text file
            prizes.SaveToPrizeFile();
        }

        public void CreatePerson(PersonModel model)
        {
            List<PersonModel> people = GlobalConfig.PeopleFile.FullFilePath().LoadFile().ConvertToPersonModels();
            var person = people.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = person == null ? 1 : person.Id + 1;
            model.Id = currentId;
            people.Add(model);
            people.SaveToPeopleFile();
        }

        public List<PersonModel> GetPerson_All()
        {
            return GlobalConfig.PeopleFile.FullFilePath().LoadFile().ConvertToPersonModels();
        }

        public void CreateTeam(TeamModel model)
        {
            List<TeamModel> teams = GlobalConfig.TeamsFile.FullFilePath().LoadFile().ConvertToTeamModels();
            var lastTeam = teams.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = lastTeam == null ? 1 : lastTeam.Id + 1;
            model.Id = currentId;
            teams.Add(model);
            teams.SaveToTeamsFile(GlobalConfig.TeamsFile);
        }

        public List<TeamModel> GetTeam_All()
        {
            return GlobalConfig.TeamsFile.FullFilePath().LoadFile().ConvertToTeamModels();
        }

        public void CreateTournament(TournamentModel model)
        {
            List<TournamentModel> tournaments = GlobalConfig.TournamentsFile.FullFilePath().LoadFile().ConvertToTournamentModels();
            var lastTournament = tournaments.OrderByDescending(x => x.Id).FirstOrDefault();
            int currentId = lastTournament == null ? 1 : lastTournament.Id + 1;
            model.Id = currentId;
            tournaments.Add(model);
            model.SaveToRoundsFile(GlobalConfig.MatchupsFile, GlobalConfig.MatchupEntriesFile);
            tournaments.SaveToTournamentsFile(GlobalConfig.TournamentsFile);
            TournamentLogic.UpdateTournamentResults(model);
        }

        public List<TournamentModel> GetTournament_All()
        {
            return GlobalConfig.TournamentsFile.FullFilePath().LoadFile().ConvertToTournamentModels();
        }

        public void UpdateMatchup(MatchupModel model)
        {
            model.UpdateMatchupToFile();
        }

        public void CompleteTournament(TournamentModel model)
        {
            List<TournamentModel> tournaments = GlobalConfig.TournamentsFile.FullFilePath().LoadFile().ConvertToTournamentModels();
            // delete the completed tournament from file
            tournaments.Remove(model);
            model.SaveToRoundsFile(GlobalConfig.MatchupsFile, GlobalConfig.MatchupEntriesFile);
            tournaments.SaveToTournamentsFile(GlobalConfig.TournamentsFile);
            TournamentLogic.UpdateTournamentResults(model);
        }
    }
}
