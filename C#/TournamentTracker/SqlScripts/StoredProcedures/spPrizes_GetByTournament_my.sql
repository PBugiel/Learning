
CREATE PROCEDURE dbo.spPrizes_GetByTournamentId
	@TournamentId int
AS
BEGIN
	SET NOCOUNT ON;
	select p.* from dbo.Prizes p
	inner join dbo.TournamentPrizes t on p.id = t.PrizeId
	where t.TournamentId = @TournamentId;
END
GO
