﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Models;

namespace TrackerUI
{
    public partial class TournamentDashboardForm : Form, ITournamentRequester
    {
        List<TournamentModel> existingTournaments = GlobalConfig.Connection.GetTournament_All();
        //TournamentModel activeTournament = new TournamentModel();
        public TournamentDashboardForm()
        {
            InitializeComponent();
            WireUpLists();
        }

        private void WireUpLists()
        {
            loadExistingTournamentDropDown.DataSource = null;
            loadExistingTournamentDropDown.DataSource = existingTournaments;
            loadExistingTournamentDropDown.DisplayMember = "TournamentName";
        }

        private void createTournamentButton_Click(object sender, EventArgs e)
        {
            CreateTournamentForm createTournamentForm = new CreateTournamentForm(this);
            createTournamentForm.Show();
        }

        public void TournamentComplete(TournamentModel tournament)
        {
            existingTournaments.Add(tournament);
            //activeTournament = tournament;
            WireUpLists();
            TournamentViewerForm viewer = new TournamentViewerForm(tournament);
            viewer.Show();
        }

        private void loadTournamentButton_Click(object sender, EventArgs e)
        {
            TournamentModel tm = (TournamentModel)loadExistingTournamentDropDown.SelectedItem;
            TournamentViewerForm viewer = new TournamentViewerForm(tm);
            viewer.Show();
        }
    }
}
