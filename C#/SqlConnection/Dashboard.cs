﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SqlConnection
{
    public partial class sqlConnectionForm : Form
    {
        List<Person> people = new List<Person>();
        public sqlConnectionForm()
        {
            InitializeComponent();
            UpdateBinding();
        }

        private void UpdateBinding()
        {
            peopleFoundListBox.DataSource = people;
            peopleFoundListBox.DisplayMember = "FullInfo";
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            // this should be somewhere in the 'backend' code - library
            DataAccess db = new DataAccess();
            people = db.GetPeople(lastNameText.Text);
            // people list was updated, but now also bindings need to be updated
            UpdateBinding();
        }

        private void insertRecordButton_Click(object sender, EventArgs e)
        {
            // this should be somewhere in the 'backend' code - library
            DataAccess db = new DataAccess();
            db.InsertPerson(firstNameInsText.Text, lastNameInsText.Text, emailAddressInsText.Text, phoneNumberInsText.Text);
            // people list was updated, but now also bindings need to be updated
            firstNameInsText.Text = "";
            lastNameInsText.Text = "";
            emailAddressInsText.Text = "";
            phoneNumberInsText.Text = "";
        }
    }
}
