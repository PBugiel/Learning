﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace SqlConnection
{
    public class DataAccess
    {
        public List<Person> GetPeople(string lastName)
        {
            var connString = Helper.ConnVal("MyFirstDb");
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connString))
            {
                // Query = ask for data
                // <People> what type of data
                // "select... " command to run
                // !! raw SQL is not recommended !! hence the second variation
                //var output = connection.Query<Person>($"select * from People where LastName = '{lastName}'").ToList();

                // spGetByLastName is a stored procedure in People table; @LastName is a property read from a class instance
                // new { LastName = lastName } creates and instance of a dynamic class
                // The data from Person table is matched to Person class by Dapper by name (table column <=> property)
                var output = connection.Query<Person>("dbo.spPeople_GetByLastName @LastName", new { LastName = lastName }).ToList();
                return output;
            }
        }

        internal void InsertPerson(string firstName, string lastName, string emailAddress, string phoneNumber)
        {
            var connString = Helper.ConnVal("MyFirstDb");
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connString))
            {
                //Person newPerson = new Person { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, PhoneNumber = phoneNumber };
                List<Person> people = new List<Person>();
                people.Add(new Person { FirstName = firstName, LastName = lastName, EmailAddress = emailAddress, PhoneNumber = phoneNumber });
                connection.Execute("dbo.spPeople_Insert @FirstName, @LastName, @EmailAddress, @PhoneNumber", people);
            }
        }
    }
}
