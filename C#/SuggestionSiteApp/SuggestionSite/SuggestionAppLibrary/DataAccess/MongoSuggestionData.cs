﻿using Microsoft.Extensions.Caching.Memory;

namespace SuggestionAppLibrary.DataAccess;
public class MongoSuggestionData : ISuggestionData
{
    IMongoCollection<SuggestionModel> _suggestions;
    private readonly IDbConnection _db;
    private readonly IUserData _userData;
    private readonly IMemoryCache _cache;
    private const string CacheName = "SuggestionData";

    public MongoSuggestionData(IDbConnection db, IUserData userData, IMemoryCache cache)
    {
        _db = db;
        _userData = userData;
        _cache = cache;
        _suggestions = db.SuggestionCollection;
    }

    public async Task<List<SuggestionModel>> GetAllSuggestions()
    {
        var output = _cache.Get<List<SuggestionModel>>(CacheName);
        if (output == null)
        {
            var results = await _suggestions.FindAsync(s => s.Archived == false);
            output = results.ToList();
            _cache.Set(CacheName, output, TimeSpan.FromMinutes(1));
        }
        return output;
    }

    public async Task<List<SuggestionModel>> GetUsersSuggestion(string userId)
    {
        // cache is shared across users, thus we have to retrieve the data belonging to userId
        var ret = _cache.Get<List<SuggestionModel>>(userId);
        if (ret is null)
        {
            var results = await _suggestions.FindAsync(s => s.Author.Id == userId);
            ret = results.ToList();
            _cache.Set(userId, ret, TimeSpan.FromMinutes(1));
        }
        return ret;
    }

    public async Task<List<SuggestionModel>> GetAllApprovedSuggestions()
    {
        var output = await GetAllSuggestions();
        return output.Where(x => x.ApprovedForRelease).ToList();
    }

    public async Task<SuggestionModel> GetSuggestion(string id)
    {
        var results = await _suggestions.FindAsync(s => s.Id == id);
        return results.FirstOrDefault();
    }

    public async Task<List<SuggestionModel>> GetAllSuggestionsWaitingForApproval()
    {
        var output = await GetAllSuggestions();
        return output.Where(x => x.ApprovedForRelease == false && x.Rejected == false).ToList();
    }

    public async Task UpdateSuggestion(SuggestionModel suggestion)
    {
        await _suggestions.ReplaceOneAsync(s => s.Id == suggestion.Id, suggestion);
        _cache.Remove(CacheName);
    }

    public async Task UpvoteSuggestion(string suggestionId, string userId)
    {
        MongoClient client = _db.Client;
        using var session = await client.StartSessionAsync();
        session.StartTransaction(); // start transaction

        try
        {
            IMongoDatabase db = client.GetDatabase(_db.DbName);
            IMongoCollection<SuggestionModel> suggestionsInTransaction = db.GetCollection<SuggestionModel>(_db.SuggestionCollectionName);
            SuggestionModel suggestion = (await suggestionsInTransaction.FindAsync(s => s.Id == suggestionId)).First();
            // suggestion.UserVotes is a HashSet<string> - a list without duplicates
            // HashSet<>.Add returns bool - false if the element already exists
            bool isUpvote = suggestion.UserVotes.Add(userId);
            // if we get 'false' - it means that the suggestion has been voted on and clicking on 'upvote'
            // means removing a users vote
            if (isUpvote == false)
            {
                suggestion.UserVotes.Remove(userId);
            }
            // update the suggestion
            await suggestionsInTransaction.ReplaceOneAsync(session, s => s.Id == suggestionId, suggestion);

            IMongoCollection<UserModel> usersInTransaction = db.GetCollection<UserModel>(_db.UserCollectionName);
            UserModel user = await _userData.GetUser(userId);
            if (isUpvote)
            {
                user.VotedOnSuggestion.Add(new BasicSuggestionModel(suggestion));
            }
            else
            {
                BasicSuggestionModel suggestionToRemove = user.VotedOnSuggestion.Where(s => s.Id == suggestionId).First();
                user.VotedOnSuggestion.Remove(suggestionToRemove);
            }
            await usersInTransaction.ReplaceOneAsync(session, u => u.Id == userId, user);

            await session.CommitTransactionAsync(); // end transaction

            _cache.Remove(CacheName);
        }
        catch (Exception)
        {
            await session.AbortTransactionAsync();
            throw; // throw errors up to the caller
        }
    }

    public async Task CreateSuggestion(SuggestionModel suggestion)
    {
        MongoClient client = _db.Client;
        using var session = await client.StartSessionAsync();
        try
        {
            session.StartTransaction();
        }
        catch (Exception e)
        {
            await Console.Out.WriteLineAsync("Exception: " + e);
            await session.AbortTransactionAsync();
        }

        try
        {
            IMongoDatabase db = client.GetDatabase(_db.DbName);
            IMongoCollection<SuggestionModel> suggestionsInTransaction = db.GetCollection<SuggestionModel>(_db.SuggestionCollectionName);
            await suggestionsInTransaction.InsertOneAsync(suggestion);

            IMongoCollection<UserModel> usersInTransaction = db.GetCollection<UserModel>(_db.UserCollectionName);
            UserModel user = await _userData.GetUser(suggestion.Author.Id);
            user.AuthoredSuggestions.Add(new BasicSuggestionModel(suggestion));
            await usersInTransaction.ReplaceOneAsync(u => u.Id == user.Id, user);

            await session.CommitTransactionAsync();
            // not deleting cache => the suggestion will appear (to the admins) after cache timeout (1min)
        }
        catch (Exception)
        {
            await session.AbortTransactionAsync();
            throw;
        }
    }
}
