namespace SuggestionSiteUI.Pages;

public partial class Profile
{
    private UserModel loggedInUser;
    private List<SuggestionModel> submissions;
    private List<SuggestionModel> approved;
    private List<SuggestionModel> archived;
    private List<SuggestionModel> pending;
    private List<SuggestionModel> rejected;
    protected async override Task OnInitializedAsync()
    {
        loggedInUser = await authProvider.GetUserFromAuth(userData);
        var userSuggestions = await suggestionData.GetUsersSuggestion(loggedInUser.Id);
        if (loggedInUser is not null && userSuggestions is not null)
        {
            // creating separate lists - lists are just references so the addotional 
            // memory consumption is negligible
            submissions = userSuggestions.OrderByDescending(s => s.DateCreated).ToList();
            approved = submissions.Where(s => s.ApprovedForRelease && !s.Archived && !s.Rejected).ToList();
            archived = submissions.Where(s => s.Archived && !s.Rejected).ToList();
            pending = submissions.Where(s => !s.ApprovedForRelease && !s.Rejected).ToList();
            rejected = submissions.Where(s => s.Rejected).ToList();
        }
    }

    private void ClosePage()
    {
        navManager.NavigateTo("/");
    }
}