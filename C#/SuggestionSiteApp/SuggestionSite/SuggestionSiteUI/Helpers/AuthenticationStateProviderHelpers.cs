﻿using Microsoft.AspNetCore.Components.Authorization;

namespace SuggestionSiteUI.Helpers;

public static class AuthenticationStateProviderHelpers
{
    public static async Task<UserModel> GetUserFromAuth(
        this AuthenticationStateProvider provider,
        IUserData userData)
    {
        var authState = await provider.GetAuthenticationStateAsync();
        // claims are pieces of info like name, email address, display name etc. 'objectidentifier' is an unique id given by Azure
        string objectId = authState.User.Claims.FirstOrDefault(c => c.Type.Contains("objectidentifier"))?.Value;
        return await userData.GetUserFromAuthentication(objectId);
    }
}
