﻿using Dapper;
//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary
{
    // not static - can be instantiated in order not to write to database in unit tests
    public class DataAccess : IDataAccess
    {
        /// <summary>
        /// Generic method fetching data from database
        /// </summary>
        /// <typeparam name="T">Type of data to be fetched from database</typeparam>
        /// <typeparam name="U">Data type holding parameters for query</typeparam>
        /// <param name="sql">SQL statement to execute</param>
        /// <param name="parameters">Query parameters</param>
        /// <param name="connectionString">Database connection string</param>
        /// <returns>List of data fetched from database</returns>
        public async Task<List<T>> LoadData<T, U>(string sql, U parameters, string connectionString)
        {
            // MySQL: using (IDbConnection connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString))
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                var rows = await connection.QueryAsync<T>(sql, parameters);
                return rows.ToList();
            }
        }

        /// <summary>
        /// Generic method saving data to database
        /// </summary>
        /// <typeparam name="T">Data type holding parameters for query execution</typeparam>
        /// <param name="sql">SQL statement to execute</param>
        /// <param name="parameters">Query parameters</param>
        /// <param name="connectionString">Database connection string</param>
        public async Task SaveData<T>(string sql, T parameters, string connectionString)
        {
            // MySQL: using (IDbConnection connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString))
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                await connection.ExecuteAsync(sql, parameters);
            }
        }
    }
}
