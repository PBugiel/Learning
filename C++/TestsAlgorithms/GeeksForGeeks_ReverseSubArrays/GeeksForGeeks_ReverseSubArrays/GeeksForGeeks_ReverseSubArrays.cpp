#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


class Solution {
public:
    //Function to reverse every sub-array group of size k.
    void reverseInGroups(vector<long long>& arr, int n, int k) 
    {
        for (int i = 0; i < n / k; ++i)
        {
            int beg = i * k;
            int end = (i + 1) * k - 1;
            for (int j = 0; j < k / 2; ++j)
            {
                int tmp = arr[beg + j];
                arr[beg + j] = arr[end - j];
                arr[end - j] = tmp;
            }
        }
        // the rest n%k elements
        int beg = n - n % k;
        int end = n - 1;
        for (int j = 0; j < (n - beg) / 2; ++j)
        {
            int tmp = arr[beg + j];
            arr[beg + j] = arr[end - j];
            arr[end - j] = tmp;
        }
    }

    void reverseInGroups2(vector<long long>& arr, int n, int k)
    {
        for (int i = 0; i < n / k; ++i)
        {
            int beg = i * k;
            int end = (i + 1) * k;
            reverse(arr.begin() + beg, arr.begin()+end);
        }
        // the rest n%k elements
        int beg = n - n % k;
        int end = n;
        reverse(arr.begin() + beg, arr.begin() + end);
    }
};

void printVector(vector<long long> arr)
{
    for (long long elm : arr)
    {
        cout << elm << " ";
    }
    cout << endl;
}

void test1()
{
    Solution sol;
    int N = 5, K = 3;
    vector<long long> arr{ 1,2,3,4,5 };
    printVector(arr);
    sol.reverseInGroups2(arr, N, K);
    printVector(arr);
    // Output: 3 2 1 5 4
}

void test2()
{
    Solution sol;
    int N = 4, K = 3;
    vector<long long> arr{ 5,6,8,9 };
    printVector(arr);
    sol.reverseInGroups2(arr, N, K);
    printVector(arr);
    // Output: 8 6 5 9
}

int main()
{
    test1();
    test2();
}
