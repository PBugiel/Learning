#include <iostream>
#include <algorithm>
using namespace std;

class Solution
{
public:
    string reverseWord(string str)
    {
        if (str.size() <= 1 || str.size() >= 10000)
            return "";
        reverse(str.begin(), str.end());
        return str;
    }
};

void test1()
{
    string s = "Geeks";
    Solution sol;
    cout << "String: " << s << " in reverse: " << sol.reverseWord(s) << endl;
}

void test2()
{
    string s = "for";
    Solution sol;
    cout << "String: " << s << " in reverse: " << sol.reverseWord(s) << endl;
}

int main()
{
    test1();
    test2();
}