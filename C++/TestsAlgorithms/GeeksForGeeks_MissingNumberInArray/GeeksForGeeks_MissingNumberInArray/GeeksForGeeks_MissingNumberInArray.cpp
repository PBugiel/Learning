#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;


class Solution
{
public:
    int missingNumber(vector<int>& array, int n)
    {
        int expected = 1;
        sort(array.begin(), array.end());
        for (int num : array)
        {
            if (num != expected)
                return expected;
            expected++;
        }
        return expected;
    }

    // Use the formula of sum of first N natural numbers.
    int missingNumberSum(vector<int>& array, int n)
    {
        // sum of n natural numbers is given by n(n+1)/2
        int computedSum = n * (n + 1) / 2;
        int actualSum = accumulate(array.begin(), array.end(), 0);
        return computedSum - actualSum;
    }
};

void test1()
{
    int N = 5;
    vector<int> arr = { 1,2,3,5 };
    Solution sol;
    int expected = 4;
    //int ret = sol.missingNumber(arr, N);
    int ret = sol.missingNumberSum(arr, N);
    cout << "Solution: " << ret << "\nExpected: " << expected << endl;
    cout << ((ret == expected) ? "Test passed\n\n" : "Test failed\n\n");
}

void test2()
{
    int N = 10;
    vector<int> arr = { 6,1,2,8,3,4,7,10,5 };
    Solution sol;
    int expected = 9;
    //int ret = sol.missingNumber(arr, N);
    int ret = sol.missingNumberSum(arr, N);
    cout << "Solution: " << ret << "\nExpected: " << expected << endl;
    cout << ((ret == expected) ? "Test passed\n\n" : "Test failed\n\n");
}

int main()
{
    test1();
    test2();
}
