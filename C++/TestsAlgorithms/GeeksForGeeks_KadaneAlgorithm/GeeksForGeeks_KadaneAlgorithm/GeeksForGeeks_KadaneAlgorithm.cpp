// GeeksForGeeks_KadaneAlgorithm.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <math.h>

class Solution {
public:
    // arr: input array
    // n: size of array
    //Function to find the sum of contiguous subarray with maximum sum.
    long long maxSubarraySum(int arr[], int n)
    {
        long long sum = 0;
        long long maxSum = arr[0];
        for (int i = 0; i < n; i++)
        {
            sum += arr[i];
            maxSum = std::max(maxSum, sum);
            if (sum < 0)
                sum = 0;
        }
        return maxSum;
    }
};

int main()
{
    const int n = 6;
    int arr[n] = { -99, -26, 78, -50, -12, -10 };
    Solution sol;
    long long solution = sol.maxSubarraySum(arr, n);
    std::cout << "Solution: " << solution << std::endl;
}

