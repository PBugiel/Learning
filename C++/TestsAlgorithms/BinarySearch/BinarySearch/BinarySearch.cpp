#include <iostream>

class Solution {
public:
    int binarysearch(int arr[], int n, int k) 
    {
        int breakingPoint = n / 2;
        if (n == 1)
        {
            if (arr[0] == k)
                return breakingPoint;
            else
                return -1;
        }

        if (k < arr[breakingPoint])
        {
            int ret = binarysearch(arr, breakingPoint, k);
            return ret;
        }
        else
        {
            int ret = binarysearch(arr + breakingPoint, n - breakingPoint, k);
            return ret == -1 ? ret : ret + breakingPoint;
        }
    }
};


int main()
{
    Solution sol;
    const int N = 5;
    int arr[N]{ 1, 2, 3, 4, 5 };
    int K = 4;
    std::cout << "Solution: " << sol.binarysearch(arr, N, K) << std::endl;
    std::cout << "Expected: 3" << std::endl;
}
