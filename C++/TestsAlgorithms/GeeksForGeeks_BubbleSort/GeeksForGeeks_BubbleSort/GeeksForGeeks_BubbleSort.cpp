
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
using namespace std;
typedef int ll;

class Solution
{
public:
    void bubbleSort(int arr[], int n)
    {
        if (n == 0) return;
        int end = n - 1;
        while (end > 0)
        {
            for (int i = 0; i < end; ++i)
            {
                if (arr[i] > arr[i + 1])
                {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                }
            }
            end -= 1;
        }
    }
};

string printArray(int arr[], int n)
{
    string ret = "";
    for (int i = 0; i < n; ++i)
    {
        ret.append(to_string(arr[i])).append(" ");
    }
    return ret;
}

void test1()
{
    int N = 5;
    int arr[] = { 4, 1, 3, 9, 7 };
    Solution sol;
    int expected[] = { 1, 3, 4, 7, 9 };
    sol.bubbleSort(arr, N);
    cout << "Solution: " << printArray(arr, N) << "; Expected: " << printArray(expected, N) << endl;
}

void test2()
{
    int N = 10;
    int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    int expected[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    Solution sol;
    sol.bubbleSort(arr, N);
    cout << "Solution: " << printArray(arr, N) << "; Expected: " << printArray(expected, N) << endl;
}

int main()
{
    test1();
    test2();
}