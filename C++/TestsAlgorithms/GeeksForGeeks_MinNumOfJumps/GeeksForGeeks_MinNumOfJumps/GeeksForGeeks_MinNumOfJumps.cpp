#include <iostream>

class Solution {
public:
    int minJumps(int arr[], const int n)
    {
        int pos = 0, des = 0, jump = 0;

        if (n == 1)
            return 0;
        if (arr[0] == 0)
            return -1;

        for (int i = 0; i < n - 1; i++)
        {
            des = std::max(des, i + arr[i]);
            if (pos == i)
            {
                pos = des;
                jump++;
            }
        }

        if (pos < n - 1)
            return -1;
        return jump;
    }
};

int main()
{
    Solution sol;
    const int n = 15;
    int arr[n] = { 9, 10, 1, 2, 3, 4, 8, 0, 0, 0, 0, 0, 0, 0, 1 };
    int jumps = sol.minJumps(arr, n);
    std::cout << "Min number of jumps: " << jumps << std::endl << std:: endl;
}

