#include <iostream>
#include <set>

class Solution {
public:
    //Function to return the count of number of elements in union of two arrays.
    int doUnion(int a[], int n, int b[], int m) {
        std::set<int> setA(a, a + n);
        std::set<int> setB(b, b + m);
        setA.insert(setB.begin(), setB.end());
        return setA.size();
    }
};

int main()
{
    Solution sol;

    const int n = 5, m = 3;
    int a[n]{ 1, 2, 3, 4, 5 };
    int b[m]{ 1, 2, 3 };
    std::cout << "Solution: " << sol.doUnion(a, n, b, m) << std::endl;
    std::cout << "Expected: 5" << std::endl << std::endl;


    const int p = 6, q = 2;
    int c[p]{ 85, 25, 1, 32, 54, 6 };
    int d[q]{ 85, 2 };
    std::cout << "Solution: " << sol.doUnion(c, p, b, q) << std::endl;
    std::cout << "Expected: 7" << std::endl << std::endl;

}
