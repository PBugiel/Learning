#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    void sort012(int a[], int n)
    {
        if (n == 0) return;
        int l = 0;
        int m = 0;
        int h = n - 1;
        while (m <= h)
        {
            // elm at m == 0 => swap elm at m with elm at l, increment l and m
            if (a[m] == 0)
            {
                int tmp = a[l];
                a[l] = a[m];
                a[m] = tmp;
                ++l; ++m;
            }

            // elm at m == 1 => increment m
            else if (a[m] == 1)
            {
                ++m;
            }

            // elm at m == 2 => swam elm at m with elm at h, decrement h
            else
            {
                int tmp = a[h];
                a[h] = a[m];
                a[m] = tmp;
                --h;
            }
        }
    }
};

string printArray(int arr[], int n)
{
    string ret = "";
    for (int i = 0; i < n; ++i)
    {
        ret.append(to_string(arr[i])).append(" ");
    }
    return ret;
}

void test1()
{
    int N = 5;
    int arr[]{ 0, 2, 1, 2, 0 };
    Solution sol;
    int expected[] = { 0, 0, 1, 2, 2 };
    sol.sort012(arr, N);
    cout << "Solution: " << printArray(arr, N) << "; Expected: " << printArray(expected, N) << endl;
}

void test2()
{
    int N = 3;
    int arr[] = { 0, 1, 0 };
    int expected[] = { 0, 0, 1 };
    Solution sol;
    sol.sort012(arr, N);
    cout << "Solution: " << printArray(arr, N) << "; Expected: " << printArray(expected, N) << endl;
}

int main()
{
    test1();
    test2();
}
