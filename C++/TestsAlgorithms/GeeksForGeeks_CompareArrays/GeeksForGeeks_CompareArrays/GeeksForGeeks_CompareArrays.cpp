#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
typedef int ll;

class Solution
{
public:

    //Function to check if two arrays are equal or not.
    bool check(vector<ll> A, vector<ll> B, int N)
    {
        sort(A.begin(), A.end());
        sort(B.begin(), B.end());
        if (A == B)
            return true;
        else
            return false;
    }
};

void test1()
{
Input:
    int N = 5;
    vector<int> A = { 1,2,5,4,0 };
    vector<int> B = { 2,4,5,0,1 };
    Solution sol;
    bool expected = true;
    bool ret = sol.check(A, B, N);
    cout << "Solution: " << ret << "; Expected: " << expected << endl;
    if (ret == expected)
        cout << "Test passed\n";
    else
        cout << "Test FAILED\n";
}

void test2()
{
    int N = 3;
    vector<int> A = { 1,2,5 };
    vector<int> B = { 2,4,15 };
    Solution sol;
    bool expected = false;
    bool ret = sol.check(A, B, N);
    cout << "Solution: " << ret << "; Expected: " << expected << endl;
    if (ret == expected)
        cout << "Test passed\n";
    else
        cout << "Test FAILED\n";
}

int main()
{
    test1();
    test2();
}
