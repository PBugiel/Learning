#pragma once
#include <iostream>
#include <vector>


// TODO namespaces
// TODO folder for each namespace

namespace LanguageFeatures
{
	void Foreach();
	void ReferencesAndConst();
	void MultidimensionalArrays();
	void DetermineCppVersion();
	void TypedefUsing();
}

namespace Algorithms
{
	void BubbleSort();
	void RandomInts(int numElements);
}

namespace Vectors
{
	void Defining();
	void Sorting();
	void ReturningVectorFromFunction();
	void MalwareSimulation();
	void RemoveEraseIdiom();
}

namespace Containers
{
	void Tuple();
}

namespace Set
{
	void GetUniqueNames();
}

namespace StringManipulations
{
	std::string ReverseWords(const std::string& sentence);
	void IntToStringWithPadding();
	void FindSubstringInString();
}

namespace FileManipulations 
{
	bool CopyFileWithPermissions(const std::string& srcPath, const std::string& dstPath);
	void WriteBinaryFile(); 
	void ListFilesRecursively(const std::string& directory);
	std::vector<char> ReadFileContentsToVector(std::string path);
	size_t ReadAndSearchFileLineByLine(const std::vector<char>& fileContents, std::string searchInLine);
	void ListPathsInDir(std::string filePath);
}

namespace LowLevelOperations
{
	void PrintHexValue(int val, std::string name = "");
	void GetBigEndianFromLittleEndianBuffer();
	void DetermineMachineEndianness();
}

namespace SmartPointers
{
	std::weak_ptr<int> WeakPointer_GetFromSharedPointer();
	std::weak_ptr<int> WeakPointer_GetEmpty();
	void WeakPointer_LockAndUse();
	void UniquePointer();
}

namespace Virtual
{
	void VirtualFunctions();
}