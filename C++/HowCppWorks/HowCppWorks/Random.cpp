#include <vector>
#include <random>
#include <cstdint>
#include "Header.h"

namespace Algorithms
{
	void RandomInts(int numElements)
	{
		auto randomVector = std::vector<std::int32_t>(numElements, 0u);

		//auto seed = std::random_device{};
		auto gen = std::mt19937{ std::random_device{}() };
		auto dist = std::uniform_int_distribution<std::int32_t>{ -10, 10 };

		for (auto& val : randomVector)
		{
			val = dist(gen);
		}

		std::cout << "Random numbers vector: ";
		for (auto val : randomVector)
		{
			std::cout << val << " ";
		}

	}
}