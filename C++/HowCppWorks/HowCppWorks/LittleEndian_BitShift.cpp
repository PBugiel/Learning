#include "Header.h"
#include <vector>
#include <stdint.h>
#include <iomanip> // for cout stream manipulation (setfill, internall etc.)


namespace LowLevelOperations
{
    void PrintHexValue(int val, std::string name /* = "" */)
    {
        std::cout << std::showbase  // show the 0x prefix
            << std::internal        // fill between the prefix and the number
            << std::setfill('0');   // fill with 0s
        int width = sizeof(val) * 2 + 2; // each byte is written as 2 characters (sizeof(val)*2), +2 refers to '0x' at the beginning
        if (name.length()) name += ": ";
        std::cout << name << std::hex << std::setw(width) << val << std::endl;
    }

    // Getting a big endian value from buffer >>stored<< as a little endian - basically switching byte order
    void GetBigEndianFromLittleEndianBuffer()
    {
        std::vector<uint8_t> buffer = { 22 /*(0x16)*/, 209 /*(0xd1)*/, 81 /*(0x51)*/, 109 /*(0x6d)*/ };
        size_t pos = 0;
        std::cout << "Buffer values: " << std::endl;
        PrintHexValue(buffer[0]);
        PrintHexValue(buffer[1]);
        PrintHexValue(buffer[2]);
        PrintHexValue(buffer[3]);

        std::cout << "\nBit shifted values" << std::endl;
        // take the first value in buffer and treat it as a small number
        uint32_t val0 = static_cast<uint32_t>(buffer[pos]); 
        PrintHexValue(val0, "val0");  // 0x00000016

        // take the second value in buffer and treat it as a bigger number - bit shift 1 byte works on value of the number
        uint32_t val1 = static_cast<uint32_t>(buffer[pos + 1]) << 8;
        PrintHexValue(val1, "val1");  // 0x0000d100

        // take third value and bit shift 16 bits - even bigger number
        uint32_t val2 = static_cast<uint32_t>(buffer[pos + 2]) << 16;
        PrintHexValue(val2, "val2");  // 0x00510000

        // take third byte and bit shift it 24 bits - the biggest number
        uint32_t val3 = static_cast<uint32_t>(buffer[pos + 3]) << 24;
        PrintHexValue(val3, "val3");  // 0x6d000000

        // now combine those numbers - this is all independent of endianness - just in terms of value
        uint32_t val = val0 | val1 | val2 | val3;
        std::cout << "Combined value with reversed bytes" << std::endl;
        PrintHexValue(val);  // 0x6d51d116

        // This would be the other way round if the value was >>stored<< in big endian
    }

    /// ==== Determinig endianness ====
    // A character pointer c is pointing to an integer i. Since size of character is 1 byte when the character pointer
    // is de-referenced it will contain only first byte of integer. If machine is little endian then *c will be 1 (because
    // last byte is stored first) and if the machine is big endian then *c will be 0. 
    void DetermineMachineEndianness()
    {
        unsigned int i = 1;
        char* c = (char*)&i;
        if (*c)
            std::cout << "Little endian";
        else
            std::cout << "Big endian";
    }
} // namespace LowLevelOperations