#include "Header.h"
#include <string>
#include <vector>


namespace StringManipulations
{
    std::string ReverseWords(const std::string& sentence) {
        std::cout << "Sentence to be reversed:\n" << sentence << std::endl;
        std::vector<std::string> words;
        std::string str = sentence;

        std::string word;
        size_t pos = 0;
        while ((pos = str.find(" ")) != std::string::npos)
        {
            word = str.substr(0, pos);
            words.push_back(word);
            str.erase(0, pos + 1);
        }

        std::string reversedSentence = str;
        for (auto rit = words.rbegin(); rit < words.rend(); rit++)
        {
            reversedSentence.append(" ");
            reversedSentence.append(*rit);
        }

        std::cout << "\nReversed sentence:\n" << reversedSentence << std::endl;

        return reversedSentence;
    }
}  // namespace StringManipulations