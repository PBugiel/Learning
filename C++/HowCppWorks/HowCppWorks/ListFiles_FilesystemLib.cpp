#include "Header.h"

#ifndef __cplusplus
#	error C++ is required
#elif __cplusplus >= 201703L
#	include <filesystem>
	namespace fs = std::filesystem;
#else
#	define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING 1;
#	include <experimental/filesystem>
	namespace fs = std::experimental::filesystem;
#endif

// Add in CMakeLists.txt:
// link_libraries(stdc++fs)

namespace FileManipulations
{
	void ListPathsInDir(std::string filePath)
	{
		// List paths in dir
		std::vector<fs::path> paths;
		for (fs::recursive_directory_iterator i(filePath), end; i != end; ++i)
		{
			// look for .odx and .dt files that are not dierectories
			if ((!is_directory(i->path())) && ((i->path().extension() == ".odx") || ((i->path().extension() == ".dt"))))
				paths.push_back(i->path().u8string());
		}
	}
}  // namespace FileManipulations