#include "Header.h"

#include <sstream>
#include <vector>
#include <string>


namespace FileManipulations
{
    // Function reads file line by line searching for a substring, and counts bytes read
    size_t ReadAndSearchFileLineByLine(const std::vector<char>& fileContents, std::string searchInLine)
    {
        std::stringstream contentsStream(std::string(fileContents.begin(), fileContents.end()));
        std::string line;
        size_t bytesRead = 0;
        size_t ret = 0;

        while (std::getline(contentsStream, line))
        {
            bytesRead += line.size() + 1;
            if (line.find(searchInLine) != std::string::npos)
            {
                ret = bytesRead;
                std::cout << "Found the string: " << searchInLine << std::endl
                    << "Total bytes read (including the found line): " << bytesRead << std::endl;
                return ret;
            }
        }
        std::cout << "Could not find the string: " << searchInLine << std::endl;
        return ret;
    }
}  // namespace FileManipulations