#include "Helpers.h"

void PrintVector(const std::vector<int>& v, std::string title /* = "" */)
{
	std::cout << "Vector " << title << std::endl;
	for (const int& elm : v) std::cout << elm << " ";
	std::cout << std::endl;
}