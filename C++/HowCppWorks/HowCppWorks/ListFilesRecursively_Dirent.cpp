#include "Header.h"
#include <vector>

namespace FileManipulations
{
#ifdef __linux__ 
#include <dirent.h>
    void ListFilesRecursively(const std::string& directory)
    {
        std::vector<std::string> filePaths;
        std::string directoryToList = directory;
        if (directoryToList.find_last_of('/') != directoryToList.length() - 1)
        {
            directoryToList = directoryToList.append("/");
        }
        DIR* dir;
        struct dirent* ent;
        if ((dir = opendir(directoryToList.c_str())) != NULL)
        {
            while ((ent = readdir(dir)) != NULL)
            {
                if (std::string(ent->d_name).compare(".") != 0 && std::string(ent->d_name).compare("..") != 0)
                {
                    if (ent->d_type == 4) // it's a directory
                    {
                        listFilesRecursively(directoryToList + std::string(ent->d_name), filePaths);
                    }
                    else if (ent->d_type == 8) // a regular file
                    {
                        filePaths.push_back(directoryToList + std::string(ent->d_name));
                    }
                }

            }
            closedir(dir);
        }
        for (std::string& filePath : filePaths)
        {
        }

    }
#elif _WIN32
    void ListFilesRecursively(const std::string& directory)
    {
        std::cout << "ListFilesRecursively not implemented for Windows" << std::endl;

        std::vector<std::string> filePaths;
    }

#else
    void ListFilesRecursively(const std::string& directory)
    {
        std::cout << "ListFilesRecursively not implemented for other platforms" << std::endl;
    }
#endif
} // namespace FileManipulations