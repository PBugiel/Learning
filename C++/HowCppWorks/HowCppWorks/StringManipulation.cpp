#include "Header.h"
#include <iomanip> // setw, setfill
#include <sstream> // stringstream
#include <string> 

namespace StringManipulations
{
	void IntToStringWithPadding()
	{
	int sequence_number{ 11 };
	std::stringstream ss;
	ss << std::setw(8) << std::setfill('0') << sequence_number;
	std::string str = ss.str();
	std::cout << str << std::endl;
	}

	void FindSubstringInString()
	{
		std::string aString = "Some long string";
		std::string substr = "long";
		size_t foundIdx = aString.find(substr);
		std::cout << "String to search in : '" << aString << "'\n";
		std::cout << "Substring to search : '" << substr << "'\n";
		if (foundIdx != std::string::npos)
		{
			std::cout << "First occurence is found at index " << foundIdx << std::endl;
		}
		else
		{
			std::cout << "Substring not found" << std::endl;
		}
	}
}  // namespace StringManipulations