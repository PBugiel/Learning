#include "Header.h"
#include <array>
#include <vector>
#include <map>
#include <cstdint>
#include <memory>
#include <functional>
#include <any>
#include <type_traits>
#include <cstring>


// TODO find a way to use the templates
namespace Templates
{
    class ValueProps
    {
    public:
        ValueProps(std::any value, int typeId, size_t valueSize) : value(value), /*typeId(typeId),*/ valueSize(valueSize) {}
        std::any value;
        //int typeId;
        size_t valueSize;
    };

    using AnyValue2BuffCast = std::function<std::vector<uint8_t>(const std::any&, uint16_t)>;
    using Buff2AnyValue = std::function<ValueProps(const std::vector<uint8_t>&)>;

    template <typename T>
    class has_serialize
    {
        typedef char yes;
        typedef struct { char array[2]; } no;

        template <typename C> static yes test(decltype(&C::serialize));
        template <typename C> static no  test(...);

    public:
        enum { value = sizeof(test<T>(0)) == sizeof(char) };
    };

    template <typename T>
    class has_deserialize
    {
        typedef char yes;
        typedef struct { char array[2]; } no;

        template <typename C> static yes test(decltype(&C::deserialize));
        template <typename C> static no  test(...);

    public:
        enum { value = sizeof(test<T>(0)) == sizeof(char) };
    };

    template<typename T>
    struct has_const_iterator
    {
    private:
        typedef char yes;
        typedef struct { char array[2]; } no;

        template<typename C> static yes test(typename C::const_iterator*);
        template<typename C> static no  test(...);
    public:
        static const bool value = sizeof(test<T>(0)) == sizeof(yes);
        typedef T type;
    };

    template <typename T>
    struct has_begin_end
    {
        template<typename C> static char(&f(typename std::enable_if<
            std::is_same<decltype(static_cast<typename C::const_iterator(C::*)() const>(&C::begin)),
            typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

        template<typename C> static char(&f(...))[2];

        template<typename C> static char(&g(typename std::enable_if<
            std::is_same<decltype(static_cast<typename C::const_iterator(C::*)() const>(&C::end)),
            typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

        template<typename C> static char(&g(...))[2];

        static bool const beg_value = sizeof(f<T>(0)) == 1;
        static bool const end_value = sizeof(g<T>(0)) == 1;
    };
    template<typename T>
    struct is_container : std::integral_constant<bool, has_const_iterator<T>::value
        && has_begin_end<T>::beg_value&& has_begin_end<T>::end_value>
    {};

    template <class T>
    struct is_map : std::false_type {};

    template<class Key, class Value>
    struct is_map<std::map<Key, Value>> : std::true_type {};

    template<typename>
    struct is_vector : std::false_type {};

    template<typename T, typename A>
    struct is_vector<std::vector<T, A>> : std::true_type {};


    class ConversionMng
    {
    private:

        template<typename T>
        void any2BytesCastingSimpleType()
        {
            auto iterAny2Cast = mAny2BytesCastHandler.find("SimpleType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        const uint8_t* data = reinterpret_cast<const uint8_t*>(std::any_cast<T>(&value));
                        return { data, std::next(data, size) };
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingComplexType()
        {
            auto iterAny2Cast = mAny2BytesCastHandler.find("ComplexType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        std::vector<uint8_t> buffer;
                        buffer.reserve(size);
                        const T* serializableObject = std::any_cast<T>(&value);
                        serializableObject->serialize(buffer);
                        return buffer;
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingObject()
        {
            if constexpr (has_serialize<T>::value)
            {
                any2BytesCastingComplexType<T>();
            }
            else
            {
                any2BytesCastingSimpleType<T>();
            }
        }

        template<typename T>
        void any2BytesCastingContainerMapComplexType()
        {
            // handler for serialization of maps containing ISerializable object
            auto iterAny2Cast = mAny2BytesCastHandler.find("MapComplexType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        std::vector<uint8_t> buffer;
                        uint16_t bufferPos = 0;
                        buffer.reserve(size + sizeof(uint16_t));
                        const T* mapOfSerializables = std::any_cast<T>(&value);
                        uint16_t mapSize = std::size(*mapOfSerializables);
                        buffer.resize(sizeof(uint16_t));
                        memcpy(&buffer[bufferPos], &mapSize, sizeof(uint16_t));
                        bufferPos += sizeof(uint16_t);
                        for (auto it = std::begin(*mapOfSerializables); it != std::end(*mapOfSerializables); it++)
                        {
                            buffer.resize(buffer.size() + sizeof(it->first));
                            memcpy(&buffer[bufferPos], &it->first, sizeof(it->first));
                            bufferPos += sizeof(it->first);
                            bufferPos += it->second.serialize(buffer);
                        }
                        return buffer;
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingContainerOtherComplexType()
        {
            // handler for serialization of array/vector containing ISerializable object
            auto iterAny2Cast = mAny2BytesCastHandler.find("OtherComplexType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        std::vector<uint8_t> buffer;
                        buffer.reserve(size + sizeof(uint16_t));
                        const T* containerOfSerializables = std::any_cast<T>(&value);
                        uint16_t containerSize = std::size(*containerOfSerializables);
                        buffer.resize(sizeof(uint16_t));
                        memcpy(&buffer[0], &containerSize, sizeof(uint16_t));
                        for (auto it = std::begin(*containerOfSerializables); it != std::end(*containerOfSerializables); it++)
                        {
                            it->serialize(buffer);
                        }
                        return buffer;
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingContainerMapSimpleType()
        {
            // handler for serialization of maps containing simple object
            auto iterAny2Cast = mAny2BytesCastHandler.find("MapSimpleType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        std::vector<uint8_t> buffer;
                        uint16_t bufferPos = 0;
                        buffer.reserve(size + sizeof(uint16_t));
                        const T* mapOfSerializables = std::any_cast<T>(&value);
                        uint16_t mapSize = std::size(*mapOfSerializables);
                        buffer.resize(sizeof(uint16_t));
                        memcpy(&buffer[bufferPos], &mapSize, sizeof(uint16_t));
                        bufferPos += sizeof(uint16_t);
                        for (auto it = std::begin(*mapOfSerializables); it != std::end(*mapOfSerializables); it++)
                        {
                            buffer.resize(buffer.size() + sizeof(it->first) + sizeof(it->second));
                            memcpy(&buffer[bufferPos], &it->first, sizeof(it->first));
                            bufferPos += sizeof(it->first);
                            memcpy(&buffer[bufferPos], &it->second, sizeof(it->second));
                            bufferPos += sizeof(it->second);
                        }
                        return buffer;
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingContainerOtherSimpleType()
        {
            // handler for serialization of array/vector containing simple object

            auto iterAny2Cast = mAny2BytesCastHandler.find("OtherSimpleType");

            if (iterAny2Cast == mAny2BytesCastHandler.cend())
            {
                mAny2BytesCastHandler.insert<T>(
                    [](const std::any& value, uint16_t size) -> std::vector<uint8_t>
                    {
                        std::vector<uint8_t> buffer;
                        uint16_t bufferPos = 0;
                        buffer.resize(size + sizeof(uint16_t));
                        const T* containerOfSimpleTypes = std::any_cast<T>(&value);
                        uint16_t containerSize = std::size(*containerOfSimpleTypes);
                        memcpy(&buffer[bufferPos], &containerSize, sizeof(uint16_t));
                        bufferPos += sizeof(uint16_t);
                        for (auto it = std::begin(*containerOfSimpleTypes); it != std::end(*containerOfSimpleTypes); it++)
                        {
                            memcpy(&buffer[bufferPos], &*it, sizeof(*it));
                            bufferPos += sizeof(*it);
                        }
                        return buffer;
                    }
                );
            }
        }

        template<typename T>
        void any2BytesCastingContainer()
        {
            if constexpr (is_map<T>::value)
            {
                // map serialization
                if constexpr (has_serialize<typename T::mapped_type>::value)
                {
                    any2BytesCastingContainerMapComplexType<T>();
                }
                else
                {
                    any2BytesCastingContainerMapSimpleType<T>();
                }
            }
            else
            {
                // vector and array serialization
                if constexpr (has_serialize<typename T::value_type>::value)
                {
                    any2BytesCastingContainerOtherComplexType<T>();
                }
                else
                {
                    any2BytesCastingContainerOtherSimpleType<T>();
                }
            }
        }

        template<typename T>
        void registerAny2BytesCasting()
        {
            if constexpr (is_container<T>::value)
            {
                any2BytesCastingContainer<T>();
            }
            else
            {
                any2BytesCastingObject<T>();
            }
        }

        template<typename T>
        void buff2AnyCastingContainerMapComplexType()
        {
            // handler for deserialization of map containing ISerializable object
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("MapComplexType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::key_type key_type;
                typedef typename T::mapped_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T mapOfDeserializedObjects;
                        uint16_t bufferPos = 0;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        bufferPos += sizeof(uint16_t);
                        for (int i = 0; i < containerSize; i++)
                        {
                            key_type key;
                            memcpy(&key, &buffer[bufferPos], sizeof(key_type));
                            bufferPos += sizeof(key_type);
                            val_type deserializable;
                            uint16_t deserializedBytes = deserializable.deserialize(buffer, bufferPos);
                            if (!deserializedBytes)
                            {
                                mapOfDeserializedObjects = T{};
                                bufferPos = 0;
                                break;
                            }
                            bufferPos += deserializedBytes;
                            mapOfDeserializedObjects.insert({ key, deserializable });
                            if (deserializedBytes >= buffer.size())
                            {
                                break;
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(mapOfDeserializedObjects, /*Type2Id::getTypeId<T>(),*/ size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainerVectorComplexType()
        {
            // handler for deserialization of vector containing ISerializable object
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("VectorComplexType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::value_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T vectorOfDeserializables;
                        uint16_t bufferPos = 0;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        bufferPos += sizeof(uint16_t);
                        vectorOfDeserializables.resize(containerSize);
                        for (int i = 0; i < containerSize; i++)
                        {
                            val_type deserializable;
                            uint16_t deserializedBytes = deserializable.deserialize(buffer, bufferPos);
                            if (deserializedBytes == 0)
                            {
                                vectorOfDeserializables = T{};
                                bufferPos = 0;
                                break;
                            }
                            bufferPos += deserializedBytes;
                            vectorOfDeserializables[i] = deserializable;
                            if (deserializedBytes >= buffer.size())
                            {
                                break;
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(vectorOfDeserializables, size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainerArrayComplexType()
        {
            // handler for deserialization of array containing ISerializable objects
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("ArrayComplexType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::value_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T arrayOfDeserializables;
                        uint16_t bufferPos = 0;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        if (std::size(arrayOfDeserializables) == containerSize)
                        {
                            bufferPos += sizeof(uint16_t);
                            for (int i = 0; i < containerSize; i++)
                            {
                                val_type deserializable;
                                uint16_t deserializedBytes = deserializable.deserialize(buffer, bufferPos);
                                if (deserializedBytes == 0)
                                {
                                    arrayOfDeserializables = T{};
                                    bufferPos = 0;
                                    break;
                                }
                                bufferPos += deserializedBytes;
                                arrayOfDeserializables[i] = deserializable;
                                if (bufferPos >= buffer.size())
                                {
                                    break;
                                }
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(arrayOfDeserializables, size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainerMapSimpleType()
        {
            // handler for deserialization of maps containing simple object
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("MapSimpleType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::key_type key_type;
                typedef typename T::mapped_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T mapOfSimpleTypes;
                        uint16_t bufferPos = 0;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        uint16_t dataToBeRead = containerSize * (sizeof(key_type) + sizeof(val_type)) + sizeof(uint16_t);
                        if (dataToBeRead <= buffer.size())
                        {
                            bufferPos += sizeof(uint16_t);
                            for (int i = 0; i < containerSize; i++)
                            {
                                key_type key;
                                memcpy(&key, &buffer[bufferPos], sizeof(key_type));
                                bufferPos += sizeof(key_type);
                                val_type value;
                                memcpy(&value, &buffer[bufferPos], sizeof(val_type));
                                bufferPos += sizeof(val_type);
                                mapOfSimpleTypes.insert({ key, value });
                                if (bufferPos >= buffer.size())
                                {
                                    break;
                                }
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(mapOfSimpleTypes, size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainerVectorSimpleType()
        {
            // handler for deserialization of vector containing simple object
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find<T>();

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::value_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T vectorOfSimpleTypes;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        uint16_t bufferPos = 0;
                        uint16_t dataToBeRead = containerSize * sizeof(val_type) + sizeof(uint16_t);
                        if (dataToBeRead <= buffer.size())
                        {
                            bufferPos += sizeof(uint16_t);
                            vectorOfSimpleTypes.resize(containerSize);
                            for (int i = 0; i < containerSize; i++)
                            {
                                val_type value;
                                memcpy(&value, &buffer[bufferPos], sizeof(val_type));
                                vectorOfSimpleTypes[i] = value;
                                bufferPos += sizeof(val_type);
                                if (bufferPos >= buffer.size())
                                {
                                    break;
                                }
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(vectorOfSimpleTypes, size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainerArraySimpleType()
        {
            // handler for deserialization of array containing simple object
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("ArraySimpleType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                typedef typename T::value_type val_type;
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T arrayOfSimpleTypes;
                        uint16_t containerSize = 0;
                        memcpy(&containerSize, &buffer[0], sizeof(uint16_t));
                        uint16_t bufferPos = 0;
                        uint16_t dataToBeRead = containerSize * sizeof(val_type) + sizeof(uint16_t);
                        if (std::size(arrayOfSimpleTypes) == containerSize && dataToBeRead <= buffer.size())
                        {
                            bufferPos += sizeof(uint16_t);
                            for (int i = 0; i < containerSize; i++)
                            {
                                val_type value;
                                memcpy(&value, &buffer[bufferPos], sizeof(val_type));
                                bufferPos += sizeof(val_type);
                                arrayOfSimpleTypes[i] = value;
                                if (bufferPos >= buffer.size())
                                {
                                    break;
                                }
                            }
                        }
                        uint16_t size = bufferPos >= sizeof(uint16_t) ? bufferPos - sizeof(uint16_t) : 0;
                        return ValueProps(arrayOfSimpleTypes, size);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingContainer()
        {
            if constexpr (is_map<T>::value)
            {
                // map deserialization
                if constexpr (has_deserialize<typename T::mapped_type>::value)
                {
                    buff2AnyCastingContainerMapComplexType<T>();
                }
                else
                {
                    buff2AnyCastingContainerMapSimpleType<T>();
                }
            }
            else if constexpr (is_vector<T>::value)
            {
                // vector deserialization
                if constexpr (has_deserialize<typename T::value_type>::value)
                {
                    buff2AnyCastingContainerVectorComplexType<T>();
                }
                else
                {
                    buff2AnyCastingContainerVectorSimpleType<T>();
                }
            }
            else
            {
                // array deserialization
                if constexpr (has_deserialize<typename T::value_type>::value)
                {
                    buff2AnyCastingContainerArrayComplexType<T>();
                }
                else
                {
                    buff2AnyCastingContainerArraySimpleType<T>();
                }
            }
        }

        template<typename T>
        void buff2AnyCastingSimpleType()
        {
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("SimpleType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        return ValueProps(*reinterpret_cast<const T*>(buffer.data()), sizeof(T));
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingComplexType()
        {
            auto iterBuff2AnyCast = mBuff2AnyCastHandler.find("ComplexType");

            if (iterBuff2AnyCast == mBuff2AnyCastHandler.cend())
            {
                mBuff2AnyCastHandler.insert<T>(
                    [](const std::vector<uint8_t>& buffer) -> ValueProps
                    {
                        T serializableObject{};
                        uint16_t deserializedBytes = serializableObject.deserialize(buffer);
                        if (deserializedBytes == 0)
                        {
                            serializableObject = T{};
                        }
                        return ValueProps(serializableObject, deserializedBytes);
                    }
                );
            }
        }

        template<typename T>
        void buff2AnyCastingObject()
        {
            if constexpr (has_serialize<T>::value)
            {
                buff2AnyCastingComplexType<T>();
            }
            else
            {
                buff2AnyCastingSimpleType<T>();
            }
        }

        template<typename T>
        void registerBuff2AnyCasting()
        {
            if constexpr (is_container<T>::value)
            {
                buff2AnyCastingContainer<T>();
            }
            else
            {
                buff2AnyCastingObject<T>();
            }
        }

    public:
        static ConversionMng& getInstance();

        /*template<typename T>
        void registerType()
        {
            registerAny2BytesCasting<T>();
            registerBuff2AnyCasting<T>();
        }*/

        //AnyValue conv(uint16_t typeId, const std::vector<uint8_t>& buffer);
        //std::vector<uint8_t> conv(const tools::AnyValue& value);

    private:
        ConversionMng() = default;
        ~ConversionMng() = default;

        std::vector<uint8_t> conv(uint16_t typeId, const std::any& value, uint16_t size);

        std::map<std::string, AnyValue2BuffCast> mAny2BytesCastHandler;
        std::map<std::string, Buff2AnyValue> mBuff2AnyCastHandler;
    };
} // namespace Templates