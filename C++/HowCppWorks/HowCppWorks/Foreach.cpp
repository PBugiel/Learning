
#include "Header.h"
#include <array>

namespace LanguageFeatures
{
	void Foreach()
	{
		// Range-based for (foreach)
		std::array<std::string, 3> strarr = { "One", "Two", "Three" };

		for (const std::string& str : strarr)
		{
			std::cout << str << std::endl;
		}
	}
}