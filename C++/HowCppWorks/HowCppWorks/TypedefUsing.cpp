#include "Header.h"

namespace LanguageFeatures
{

	template <typename T>
	void TypedefUsing()
	{
		// C-style - typedef
		typedef std::vector<std::int8_t> ByteVectorC;
		ByteVectorC byteVec;

		// C++ style - using
		using ByteVectorCpp = std::vector<uint8_t>;
		ByteVectorCpp byteVec2;

		using VecOfTypeT = std::vector<T>;
	}
}