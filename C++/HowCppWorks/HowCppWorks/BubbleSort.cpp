#pragma once
#include "Header.h"
#include "Helpers.h"

#include <vector>
#include <algorithm>

namespace Algorithms
{
    std::vector<int> BubbleSortAlgorithm(std::vector<int> nums);


    void BubbleSort()
    {
        std::vector<int> v = { 10, 9, 8, 6, 7, 2, 5, 1 };
        PrintVector(v, "before sorting");
        std::vector<int> s = BubbleSortAlgorithm(v);
        PrintVector(s, "after BubbleSort algorithm");
        std::sort(v.begin(), v.end());
        PrintVector(v, "after std::sort algorithm");
    }

    std::vector<int> BubbleSortAlgorithm(std::vector<int> nums) // passed by value, returns new vector
    {
        if (!nums.size()) return nums;
        auto end = nums.end() - 1;
        while (nums.begin() < end)
        {
            for (auto it = nums.begin(); it < end; it++)
            {
                if (*it > *(it + 1))
                {
                    auto tmp = *it;
                    *it = *(it + 1);
                    *(it + 1) = tmp;
                }
            }
            end -= 1;
        }
        return nums;
    }
	
	void BubbleSortArrayInPlace(int arr[], int n)
    {
        if (n == 0) return;
        int end = n - 1;
        while (end > 0)
        {
            for (int i = 0; i < end; ++i)
            {
                if (arr[i] > arr[i + 1])
                {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                }
            }
            end -= 1;
        }
    }
}  // namespace Algorithms