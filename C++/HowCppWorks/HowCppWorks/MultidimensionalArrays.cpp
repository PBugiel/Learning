#include "Header.h"


namespace LanguageFeatures
{
    void MultidimensionalArrays()
    {
        // ----- 2D Arrays -----
        // 2 elements for 1st dimension, each containing 3 ints
        int test2d[2][3] = { {2, 4, 5}, {9, 0, 19} };

        std::cout << "2D array" << std::endl;
        for (int i = 0; i < 2; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                std::cout << "[" << i << "][" << j << "] = " << test2d[i][j] << std::endl;
            }
        }


        // ----- 3D Arrays -----
        // 2 elements for 1st dimension, each containing 3 elements - 4 element int arrays
        // Element 1 = { {3, 4, 2, 3}, {0, -3, 9, 11}, {23, 12, 23, 2} }
        // Element 2 = { {13, 4, 56, 3}, {5, 9, 3, 5}, {5, 1, 4, 9} }
        int test3d[2][3][4] = {
                                { {3, 4, 2, 3}, {0, -3, 9, 11}, {23, 12, 23, 2} },
                                { {13, 4, 56, 3}, {5, 9, 3, 5}, {5, 1, 4, 9} }
        };

        std::cout << "\n\n3D array" << std::endl;
        for (int i = 0; i < 2; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                for (int k = 0; k < 4; ++k)
                {
                    std::cout << "[" << i << "][" << j << "][" << k << "] = " << test3d[i][j][k] << std::endl;
                }
            }
        }
    }
}
