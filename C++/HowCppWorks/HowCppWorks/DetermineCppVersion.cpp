#include "Header.h"

//determine C++ version as preprocessor directive
#ifndef __cplusplus
#  error C++ is required
#elif __cplusplus <= 201103L
#  error C++14 is required
#endif

namespace LanguageFeatures
{
	// By default, in Visual studio, the __cplusplus macro is set (erroneously) to 199711L
	// To show the proper __cplusplus macro value:
	// 1. Open the project's Property Pages dialog box.
	// 2. Select the Configuration Properties > C/C++ > Command Line property page.
	// 3. Add /Zc:__cplusplus or /Zc:__cplusplus- to the Additional options pane.
	void DetermineCppVersion()
	{
		if (__cplusplus == 202101L) std::cout << "C++23";
		else if (__cplusplus == 202002L) std::cout << "C++20";
		else if (__cplusplus == 201703L) std::cout << "C++17";
		else if (__cplusplus == 201402L) std::cout << "C++14";
		else if (__cplusplus == 201103L) std::cout << "C++11";
		else if (__cplusplus == 199711L) std::cout << "C++98";
		else std::cout << "pre-standard C++." << __cplusplus;
		std::cout << "\n";

		// may also check if value is greater/smaller than
		if (__cplusplus >= 201103L) std::cout << "C++11 or greater\n";
	}
}