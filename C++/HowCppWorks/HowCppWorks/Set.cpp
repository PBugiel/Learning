#include "Header.h"
#include <vector>
#include <set>

namespace Set
{
    // TODO may be shorter; can std::set be initialized like std::vector(begin, end) ??
    std::vector<std::string> UniquNames(const std::vector<std::string>& names1, const std::vector<std::string>& names2)
    {
        std::set<std::string> ret;
        for (auto idx = names1.begin(); idx < names1.end(); idx++)
        {
            ret.insert(*idx);
        }
        for (auto idx = names2.begin(); idx < names2.end(); idx++)
        {
            ret.insert(*idx);
        }
        std::vector<std::string> retvec(ret.begin(), ret.end());
        return retvec;
    }

    // Given 2 vectors with names, get one vector containing unique names from both vectors (no duplicates)
    void GetUniqueNames()
    {
        std::vector<std::string> names1 = { "Ava", "Emma", "Olivia" };
        std::vector<std::string> names2 = { "Olivia", "Sophia", "Emma" };

        std::vector<std::string> result = UniquNames(names1, names2);
        for (auto element : result)
        {
            std::cout << element << ' '; // should print Ava Emma Olivia Sophia
        }
    }
} // namespace Set