#include "Header.h"

namespace Vectors
{
#include <iostream>
#include <vector>

    // TODO - it failed some test :O
    std::vector<int> simulate(const std::vector<int>& entries)
    {
        std::vector<int> ret = entries;
        auto sz = entries.size();
        for (int i = 0; i < entries.size(); ++i)
        {
            bool leftNeighborGreater = false;
            bool righttNeighborGreater = false;
            if (i >= 3 && entries[i] <= entries[i - 3])
                leftNeighborGreater = true;

            if (i <= sz - 5 && entries[i] <= entries[i + 4])
                righttNeighborGreater = true;

            if (leftNeighborGreater || righttNeighborGreater)
                ret[i] = 0;
        }
        return ret;
    }

    // Malware simulation - malware sets value V to zero if either of the T values are greater than V
    // [... N,_,_,V,_,_,_,N, ...]
    // if either N does not exist (V is close to border) - compare with the remaining value
	void MalwareSimulation()
	{
        std::vector<int> result = simulate({ 19, 2, 0, 87, 1, 40, 80, 77, 77, 77, 77 });
        for (int value : result)
        {
            std::cout << value << " ";
        }
        // Expected output
        // 19 0 0 87 0 0 0 77 77 0 0
        std::vector<int> expected = { 19, 0, 0, 87, 0, 0, 0, 77, 77, 0, 0 };

        if (result == expected)
            std::cout << "Test passed\n";
        else
            std::cout << "Test FAILED\n";
	}
} // namespace Vectors