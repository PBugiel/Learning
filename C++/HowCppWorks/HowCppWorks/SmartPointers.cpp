#include "Header.h"


namespace SmartPointers
{
	class Thing
	{
	public:
		Thing(std::string name)
		{
			_name = name;
			std::cout << "Created a Thing called " << _name << std::endl;
		}

		~Thing()
		{
			std::cout << "Freed a Thing called " << _name << std::endl;
		}

		std::string GetThingName()
		{
			return _name;
		}

	private:
		std::string _name;
	};

	std::weak_ptr<int> WeakPointer_GetFromSharedPointer()
	{
		std::shared_ptr<int> sharedPtr = std::make_shared<int>();
		// std::shared_ptr<SomeType> sharedPtr = std::shared_ptr<SomeType>(new SomeType()); // two operations instead of one
		std::weak_ptr<int> weakPtr = sharedPtr;
		return weakPtr;
	}


	std::weak_ptr<int> WeakPointer_GetEmpty()
	{
		return std::weak_ptr<int>();
	}


	void WeakPointer_LockAndUse()
	{
		std::weak_ptr<Thing> weakPtr = std::weak_ptr<Thing>(); // empty weak ptr of type Thing
		std::shared_ptr<Thing> sharedPtr = std::make_shared<Thing>("Thing instance");
		// Don't do the below:
		// std::shared_ptr<SomeType> sharedPtr = std::shared_ptr<SomeType>(new SomeType());
		// Two memory allocations instead of one!
		// First allocation - new(), second - make_shared 
		//		(shared_ptr always  allocates it's own memory block for the object and refence counting)
		weakPtr = sharedPtr; // assigning resources to the empty weak pointer
		// Weak Pointer is valid as long as it takes ownership over the resoure(via lock()) and the underlying shared_ptr is valid.
		if (std::shared_ptr<Thing> spt = weakPtr.lock())
		{
			std::cout << spt->GetThingName() << std::endl;
		}
		else
		{
			std::cout << "weakPtr is expired\n";
		}
	}

	// Unique pointer:
	// - can have only one instance (one pointer to data)
	//		=> switching ownership with std::move(<the pointer>);
	// - frees memory when going out of scope ('delete' called automatically at the end of scope/lifetime)
	// - cannot be copied (copy constructor is deleted)
	void UniquePointer()
	{
		std::cout << "There some code before using the unique_pointer\n";
		{
			// Can also be called like this:
			// std::unique_ptr<Thing> uniq(new Thing("Name"));
			// But calling make_unique is prefferer way - it is safer if an exception occurs during pointer creation
			std::unique_ptr<Thing> uniq = std::make_unique<Thing>("Something");
			std::cout << "Thing name (pointed by uniq1): " << uniq->GetThingName() << std::endl;
		}
		std::cout << "There some more code after using the unique_pointer\n";
	}
}  // namespace SmartPointers