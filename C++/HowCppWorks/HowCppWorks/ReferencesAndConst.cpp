
#include "Header.h"
namespace LanguageFeatures
{
	void ReferencesAndConst()
	{
		// int and reference
		int num = 7;
		int& num_ref = num;
		std::cout << "num: " << num << std::endl;
		num_ref = 9; //  it's ok
		std::cout << "num: " << num << ", num_ref: " << num_ref << std::endl << std::endl;

		// Consts and references
		const int num_const = 7;
		const int& num_ref_const = num_const; //  for a const value it has to be a const int& reference
		std::cout << "num_const: " << num_ref_const << std::endl;
		//num_ref_const = 9;  // ERROR you cannot assign to a variable that is const
	}
}
