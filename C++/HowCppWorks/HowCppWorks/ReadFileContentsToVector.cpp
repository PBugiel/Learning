// TODO take care of strerror deprecation warning...
#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <vector>
#include <fstream>

namespace FileManipulations
{
    std::vector<char> ReadFileContentsToVector(std::string path)
    {
        std::vector<char> fileContent;

        std::ifstream file(path, std::ios::binary | std::ios::ate);
        if (file.is_open() && file.good())
        {
            std::streamsize size = file.tellg();
            file.seekg(0, std::ios::beg);

            fileContent.resize(size);
            if (file.read(fileContent.data(), size))
            {
                std::cout << "Read contents:" << std::endl;
                for (const char& elm : fileContent) std::cout << elm;
                std::cout << std::endl;
            }
            else
            {
                std::cout << "Problem with reading the file! File path:\n" << path << std::endl;
            }
        }
        else
        {
            const char* errorStr = std::strerror(errno);
            std::cout << "Problem with opening the file! File path:\n" << path << " Error: " << errorStr << std::endl;
        }
        return fileContent;
    }
}  // namespace FileManipulations