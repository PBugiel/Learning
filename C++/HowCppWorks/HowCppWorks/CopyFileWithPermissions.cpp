#include "Header.h"


namespace FileManipulations
{
#ifdef __linux__ 
#include <fstream>
#include <sys/stat.h> // Linux only!

    bool CopyFileWithPermissions(const std::string& srcPath, const std::string& dstPath)
    {
        struct stat results;
        stat(srcPath.c_str(), &results); // reads file permissions

        PrintPermissions(results.st_mode);

        std::ifstream  src(srcPath, std::ios::binary);
        std::ofstream  dst(dstPath, std::ios::binary);

        if (!src.is_open())
            return false;
        if (!dst.is_open())
            return false;
        dst << src.rdbuf();

        src.close();
        dst.close();

        int err = chmod(dstPath.c_str(), results.st_mode); // Linux Only - changes file permissions

        if (err != 0)
            return false;

        return true;
    }


    bool PrintPermissions(mode_t st_mode)
    {
        std::cout << "Owner permissions: ";
        if (st_mode & S_IRUSR)
            std::cout << "Read ";
        if (st_mode & S_IWUSR)
            std::cout << "Write ";
        if (st_mode & S_IXUSR)
            std::cout << "Exec ";
        std::cout << std::endl;

        std::cout << "Group permissions: ";
        if (st_mode & S_IRGRP)
            std::cout << "Read ";
        if (st_mode & S_IWGRP)
            std::cout << "Write ";
        if (st_mode & S_IXGRP)
            std::cout << "Exec ";
        std::cout << std::endl;

        std::cout << "Others permissions: ";
        if (st_mode & S_IROTH)
            std::cout << "Read ";
        if (st_mode & S_IWOTH)
            std::cout << "Write ";
        if (st_mode & S_IXOTH)
            std::cout << "Exec ";
        std::cout << std::endl;
    }

    #elif _WIN32
    bool CopyFileWithPermissions(const std::string& srcPath, const std::string& dstPath)
    {
        std::cout << "Copying file with permissions not implemented for Windows operating system." << std::endl;
        return false;
    }

    #else
    bool CopyFileWithPermissions(const std::string& srcPath, const std::string& dstPath)
    {
        std::cout << "Copying file with permissions not implemented for other operating systems." << std::endl;
        return false;
    }
    #endif
}  // namespace FileManipulations