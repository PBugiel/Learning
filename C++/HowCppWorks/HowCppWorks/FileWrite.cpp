#include "Header.h"
#include <fstream>


namespace FileManipulations
{
    void WriteBinaryFile()
    {
        std::string fileName = "File.bin";  // will be saved in executable directory
        std::ofstream dumpFile{ fileName, std::ios::binary };
        using namespace std::string_literals;  // allows for eg. null character \0 to be present in string (normally, strings are null-retminated) 
        std::string payload = "A binary file containig whatever content \0 there may be"s;  // 's' at the end indicates a literal string (allowing \0 etc)
        long sequenceNumber = 12854000;
        int apiVersion = 12;
        size_t payloadSize = payload.size();  // size changes depending on the usage of string literals; in this example it changes from 41 to 55 if 's' is used
        if (dumpFile.is_open() && dumpFile.good())
        {
            dumpFile.write(reinterpret_cast<char*>(&sequenceNumber), sizeof(sequenceNumber));
            dumpFile.write(reinterpret_cast<char*>(&apiVersion), sizeof(apiVersion));
            dumpFile.write(reinterpret_cast<char*>(&payloadSize), sizeof(payloadSize));
            if (payload.length())
            {
                dumpFile.write(payload.c_str(), payloadSize);
            }
            dumpFile.close();
            std::cout << "File (" << fileName << ") has been written to working direcotry" << std::endl;
        }
        else
        {
            std::cout << "Something went wrong and the file couldn't be written" << std::endl;
        }
    }
} // namespace FileManipulations
