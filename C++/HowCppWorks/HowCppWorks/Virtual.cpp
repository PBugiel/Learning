#include "Header.h"

namespace Virtual
{
    class A {
    public:
        virtual std::string fun() {
            return "base ";
        }
    };
    class B : public A {
    public:
        std::string fun() {
            return "derived ";
        }
    };
    void VirtualFunctions() {
        A* a = new A;
        A* b = new B;
        std::cout << "Running type A: " << a->fun() << std::endl;
        std::cout << "Running type B: " << b->fun() << std::endl;
    }
} // namespace Virtual
