#pragma once
#include "Header.h"
#include "Helpers.h"

#include <vector>
#include <algorithm> // std::sort, std::remove

namespace Vectors
{
	std::vector<int> MoveOnReturn();
	// TODO Vectors - iterators, reverse iterators, memory allocation (reserve) etc
	// TODO Vectors - more declarations and definitions

	void Defining()
	{
		// Declaring/defining with values
		std::vector<int> v = { 10, 9, 8, 6, 7, 2, 5, 1 };

	}

	void Sorting()
	{
		std::vector<int> v = { 10, 9, 8, 6, 7, 2, 5, 1 };
		std::sort(v.begin(), v.end());
	}

	// Preferred way, since C++11 - return by value (std::vector has move - semantics, which means that a local vector
	// declared in a function will be moved on return and in some cases even the move can be elided by the compiler)
	void ReturningVectorFromFunction()
	{
		std::vector<int> vec = MoveOnReturn();  // same address
		PrintVector(vec, "in caller");
		std::cout << "Address in caller: " << &vec[0] << std::endl;
		std::cout << "Since C++11 returning by value is the preferred way" << std::endl;
	}


	std::vector<int> MoveOnReturn()
	{
		std::vector<int> vec = { 7, 5, 3, 2, 9 };
		PrintVector(vec, "in function");
		std::cout << "Address in func:   " << &vec[0] << std::endl;
		return vec; // equivalent to "return std::move(vec)"
	}

	void RemoveEraseIdiom()
	{
		std::vector<int> vec = { -1, 2, -3, 3, -1, 1, 8, -9, 2, 6 };
		PrintVector(vec, "Initial vector");

		auto remove_it = std::remove(vec.begin(), vec.end(), -1);
		vec.erase(remove_it, vec.end());
		PrintVector(vec, "After removing -1 from the vector");

		auto remove_it2 = std::remove_if(vec.begin(), vec.end(), [](const auto i) {return i < 3; });
		vec.erase(remove_it2, vec.end());
		PrintVector(vec, "After removing everythig smaller than 3");
	}
}  // namespace Vectors