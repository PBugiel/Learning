#include "Header.h"

#include <tuple>
#include <string>
#include <cstdint>
#include <iostream>

namespace Containers
{
	void Tuple()
	{
		auto myTuple = std::tuple<std::int32_t, std::string, float>{2024, "Description", 2.5f};
		const auto& [i, s, f] = myTuple;

		std::cout << "i: " << i << ", s: " << s << ", f: " << f << std::endl;
	}
}