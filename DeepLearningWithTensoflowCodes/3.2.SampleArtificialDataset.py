# activate Tensorflow
import numpy as np


# Generate synthetic data
N = 100
w_true = 5
b_true = 2
noise_scale = .1
x_np = np.random.rand(N, 1)
noise = np.random.normal(scale=noise_scale, size=(N,1))
# Convert shape of y_np to (N,)
y_np = np.reshape(w_true * x_np + b_true + noise, (-1))

# Create and show plot
plt.scatter(x_np, y_np)
plt.xlabel("X")
plt.ylabel("Y")
plt.xlim(0,1)
plt.title("Toy Linear Regression Data, " r"$y = 5x + 2 + N(0,1)$")
plt.show() # plt.savefig("C:\GitPhD\PhD\Tensorflow\DeepLearningWithTensoflowCodes\SampleArtificialData")