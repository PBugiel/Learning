# Learning

## Description
Code base for different tutorials, courses and test projects that I use for learning various technologies. Includes neural networks, programming languages and technologies like Qt, HTML, Blazor etc.

## Authors and acknowledgment
Since some of the codes are tutorial/course outcomes, I am not the originator. However, all the codes were written by me (no copy paste here).
