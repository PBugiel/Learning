#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QJsonDocument>
#include <QtGlobal>
#include <QtDebug>
#include <QJsonObject>
#include <QSettings>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    dataFilePath = "<DATA_file_path>";
    cfgFilePath = "<CFG_file_path>";
    logDirName = "<LOG_directory>";
    logDirNameDefault = logDirName;
    logFileName = "<LOG_file_name>";
    QFileInfo applicationFileInfo = QFileInfo(QApplication::applicationFilePath());
    settingsFilePath = QFileInfo(applicationFileInfo.absoluteDir(), applicationFileInfo.completeBaseName() + ".ini").absoluteFilePath();

    ui->setupUi(this);
    ui->data_save_btn->setEnabled(false);
    ui->data_edit->setReadOnly(true);

    ui->cfg_save_btn->setEnabled(false);
    ui->cfg_edit->setReadOnly(true);

    //fillFromJson("constant_settings.json");
    loadSettings();
    setLocalVariablesTextOnLineEdits();
    ui->cmd_tedit->setPlainText(getDarknetCmd());
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}

QString MainWindow::getDarknetCmd()
{
    QString darknetCmd = constDarknetCmdRecipe.replace("<const_darknet_exe_path>", constDarkentExePath).replace("<data_file_path>", dataFilePath);
    darknetCmd = darknetCmd.replace("<cfg_file_path>", cfgFilePath).replace("<const_start_weights_file_path>", constStartWeightsFilePath);
    darknetCmd = darknetCmd.replace("<log_dir>", logDirName).replace("<log_name>", logFileName);
    return darknetCmd;
}

bool MainWindow::fillFromJson(QString filePath)
{
    QFileInfo check_file(filePath);
    if (!(check_file.exists() && check_file.isFile()))
        return false;
    QString content;
    QFile file;
    file.setFileName(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    content = file.readAll();
    file.close();
    QJsonDocument jdoc = QJsonDocument::fromJson(content.toUtf8());
    QJsonObject jobj = jdoc.object();
    QJsonValue jval = jobj.value(QString("command_recipe"));
    this->constDarknetCmdRecipe = jval.toString();
    jval = jobj.value(QString("const_darknet_exe_path"));
    this->constDarkentExePath = jval.toString();
    jval = jobj.value(QString("const_start_weights_file_path"));
    this->constStartWeightsFilePath = jval.toString();
    return true;
}

void MainWindow::loadSettings()
{
    QSettings setting(settingsFilePath, QSettings::IniFormat);
    setting.beginGroup("MainWindow");
    constDarkentExePath = setting.value("darknet_exe_path", "./darknet").toString();
    constStartWeightsFilePath = setting.value("start_weights_file_path", "darknet19_448.conv.23").toString();
    constDarknetCmdRecipe = setting.value("command_recipe", "<const_darknet_exe_path> detector train <data_file_path> <cfg_file_path> <const_start_weights_file_path> -dont_show >> <log_dir>/<log_name> 2>&1").toString();
    setting.endGroup();
}

void MainWindow::saveSettings()
{
    QSettings setting(settingsFilePath, QSettings::IniFormat);
    setting.beginGroup("MainWindow");
    setting.setValue("darknet_exe_path", constDarkentExePath);
    setting.setValue("start_weights_file_path", constStartWeightsFilePath);
    setting.setValue("command_recipe", constDarknetCmdRecipe);
    setting.endGroup();
}

void MainWindow::resetSettings()
{
    QSettings setting(settingsFilePath, QSettings::IniFormat);
    setting.clear();
    setting.beginGroup("MainWindow");
    constDarkentExePath = setting.value("darknet_exe_path", "./darknet").toString();
    constStartWeightsFilePath = setting.value("start_weights_file_path", "darknet19_448.conv.23").toString();
    constDarknetCmdRecipe = setting.value("command_recipe", "<const_darknet_exe_path> detector train <data_file_path> <cfg_file_path> <const_start_weights_file_path> -dont_show >> <log_dir>/<log_name> 2>&1").toString();
    setting.endGroup();
}

void MainWindow::parseDataFileContent()
{
    QString dataContent = ui->data_txt->toPlainText();
    QStringList dataLines = dataContent.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);
    for (QString line: dataLines)
    {
        if(line.contains("train") && line.indexOf("train") == 0)
        {
            QString trainFilePath = line.remove(" ").remove("train").remove("=");
            QFileInfo trainFileInfo = trainFilePath;
            QString trainingName = trainFileInfo.baseName().remove(".txt");
            if (!trainingName.isEmpty())
            {
                logFileName = trainingName.append(".log");
                ui->log_file_edit->setText(logFileName);
            }
        }
        else if(line.contains("backup") && line.indexOf("backup") == 0)
        {
            QString backupDir = line.remove(" ").remove("backup").remove("=");
            if (!backupDir.isEmpty())
            {
                logDirNameDefault = backupDir;
                logDirName = backupDir;
                ui->log_edit->setText(logDirName);
            }
        }
    }
}

void MainWindow::getLocalVariablesFromLineEdits()
{
    this->constDarkentExePath = ui->darknet_path_ledit->text();
    this->constStartWeightsFilePath = ui->start_weights_ledit->text();
}

void MainWindow::setLocalVariablesTextOnLineEdits()
{
    ui->darknet_path_ledit->setText(this->constDarkentExePath);
    ui->start_weights_ledit->setText(this->constStartWeightsFilePath);
}

void MainWindow::on_data_browse_btn_clicked()
{
    dataFilePath = QFileDialog::getOpenFileName(this,
        tr("Open Data file"), "", tr("Data Files (*.data)"));

    QFile file(dataFilePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
           return;

    ui->data_edit->setText(dataFilePath);
    QTextStream in(&file);
    ui->data_txt->setText(in.readAll());
    ui->data_save_btn->setEnabled(false);

    ui->cmd_tedit->setPlainText(getDarknetCmd());
}

void MainWindow::on_data_txt_textChanged()
{
    ui->data_save_btn->setEnabled(true);
    parseDataFileContent();
}

void MainWindow::on_data_save_btn_clicked()
{
    QFile file(dataFilePath);
    if(!file.open(QIODevice::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Can't save file: " + file.errorString());
        return;
    }
    QTextStream out(&file);
    out << ui->data_txt->toPlainText();
    ui->data_save_btn->setEnabled(false);
}

void MainWindow::on_cfg_browse_btn_clicked()
{
    cfgFilePath = QFileDialog::getOpenFileName(this,
        tr("Open CFG file"), "", tr("CFG Files (*.cfg)"));

    QFile file(cfgFilePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
           return;

    ui->cfg_edit->setText(cfgFilePath);
    QTextStream in(&file);
    ui->cfg_txt->setText(in.readAll());
    ui->cfg_save_btn->setEnabled(false);

    ui->cmd_tedit->setPlainText(getDarknetCmd());
}

void MainWindow::on_cfg_txt_textChanged()
{
    ui->cfg_save_btn->setEnabled(true);
}

void MainWindow::on_cfg_save_btn_clicked()
{
    QFile file(cfgFilePath);
    if(!file.open(QIODevice::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Can't save file: " + file.errorString());
        return;
    }
    QTextStream out(&file);
    out << ui->cfg_txt->toPlainText();
    ui->cfg_save_btn->setEnabled(false);
}

void MainWindow::on_log_browse_btn_clicked()
{
    logDirName = QFileDialog::getExistingDirectory(this, tr("Select training LOG file tartget directory"));
    ui->log_edit->setText(logDirName);
}

// TODO on logDirectory and logFileName text changed

void MainWindow::on_log_edit_textChanged(const QString &arg1)
{
    logDirName = arg1;
    ui->cmd_tedit->setPlainText(getDarknetCmd());
}

void MainWindow::on_log_file_edit_textChanged(const QString &arg1)
{
    logFileName = arg1;
    ui->cmd_tedit->setPlainText(getDarknetCmd());
}

void MainWindow::on_actionSave_triggered()
{
    QMessageBox::warning(this, "Oops!", "Function not implemented");
}

void MainWindow::on_log_default_btn_clicked()
{
    ui->log_edit->setText(logDirNameDefault);
    logDirName = logDirNameDefault;
}

void MainWindow::on_pushButton_clicked()
{
    resetSettings();
}

void MainWindow::on_apply_changes_btn_clicked()
{
    getLocalVariablesFromLineEdits();
}
