#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_data_browse_btn_clicked();

    void on_data_txt_textChanged();

    void on_data_save_btn_clicked();

    void on_cfg_browse_btn_clicked();

    void on_cfg_save_btn_clicked();

    void on_cfg_txt_textChanged();

    void on_log_browse_btn_clicked();

    void on_log_edit_textChanged(const QString &arg1);

    void on_log_file_edit_textChanged(const QString &arg1);

    void on_actionSave_triggered();

    void on_log_default_btn_clicked();

    void on_pushButton_clicked();

    void on_apply_changes_btn_clicked();

private:
    QString getDarknetCmd();
    void loadSettings();
    void saveSettings();
    void resetSettings();
    bool fillFromJson(QString filePath);
    void parseDataFileContent();
    void getLocalVariablesFromLineEdits();
    void setLocalVariablesTextOnLineEdits();

    Ui::MainWindow *ui;
    QString dataFilePath;
    QString cfgFilePath;
    QString logDirName;
    QString logDirNameDefault;
    QString logFileName;
    QString constStartWeightsFilePath;
    QString constDarkentExePath;
    QString constDarknetCmdRecipe;
    QString jsonPath;
    QString settingsFilePath;
};

#endif // MAINWINDOW_H
