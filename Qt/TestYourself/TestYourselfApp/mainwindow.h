#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "addrecorddialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_new_record_btn_clicked();
    void on_dynamic_btn_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
