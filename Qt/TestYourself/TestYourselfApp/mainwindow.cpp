#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPushButton>
#include <QDebug>
#include <QObject>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPushButton* button = new QPushButton(this);
    button->setText("Dynamic button");
    QObject::connect(button, SIGNAL(clicked()), this, SLOT(on_dynamic_btn_clicked()));
    ui->verticalLayout->addWidget(button);
    //button->show();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_new_record_btn_clicked()
{
    AddRecordDialog addRecordDialog;
    this->hide();
    addRecordDialog.setModal(true);
    addRecordDialog.exec();
    this->show();
    // TODO Add new record to some container
}

void MainWindow::on_dynamic_btn_clicked()
{
    qDebug() << "Dynamic button was clicked";
}
