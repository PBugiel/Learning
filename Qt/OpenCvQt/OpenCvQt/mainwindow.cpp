#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // read an image
    cv::Mat image = cv::imread("C:\\GitPhD\\PhD\\Learning\\Qt\\OpenCvQt\\TestData\\images\\HU-GR_0133_20160506_014137_split_001_FID_14130_FRAME_POS.jpg", 1);
    // create image window named "My Image"
    //cv::namedWindow("My Image");
    // show the image on window
    //cv::imshow("My Image", image);

    // Select ROI
    cv::Rect2d r = cv::selectROI(image);
    // Crop image
    cv::Mat imCrop = image(r);

    // Display Cropped Image
    cv::imshow("Image", imCrop);
    cv::waitKey(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}
