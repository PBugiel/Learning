#include "firstguiwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    FirstGuiWindow w;
    w.show();

    return a.exec();
}
