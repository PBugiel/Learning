#include "firstguiwindow.h"
#include "ui_firstguiwindow.h"

FirstGuiWindow::FirstGuiWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FirstGuiWindow)
{
    ui->setupUi(this);
}

FirstGuiWindow::~FirstGuiWindow()
{
    delete ui;
}

void FirstGuiWindow::on_CoffeeButton_clicked()
{
    QString lblText = ui->label->text();
    if(lblText.size() > 30) ui->label->setText("Boom!");
    else if(!QString::compare(lblText, "Boom!")) close();
    else
    {
        lblText += "...";
        ui->label->setText(lblText);
    }
}
