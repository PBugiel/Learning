#ifndef FIRSTGUIWINDOW_H
#define FIRSTGUIWINDOW_H

#include <QMainWindow>

namespace Ui {
class FirstGuiWindow;
}

class FirstGuiWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FirstGuiWindow(QWidget *parent = nullptr);
    ~FirstGuiWindow();

private slots:
    void on_CoffeeButton_clicked();

private:
    Ui::FirstGuiWindow *ui;
};

#endif // FIRSTGUIWINDOW_H
