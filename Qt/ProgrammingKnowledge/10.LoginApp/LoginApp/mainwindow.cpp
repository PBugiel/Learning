#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap pix(":/resource/img/login.png");
    int w = ui->label_pic->width();
    int h = ui->label_pic->height();
    ui->label_pic->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
    //ui->statusBar->addPermanentWidget(ui->label_stat,1);
    //ui->statusBar->addPermanentWidget(ui->progressBar,1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_login_clicked()
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();

    if(username == "test" && password == "test")
    {
        //QMessageBox::information(this, "Login", "Username and Password are corret");
        //hide();
        //secDlg = new SecondDialog(this);
        //secDlg->show();
        ui->statusBar->showMessage("Username and Password are Correct", 3000);
    }
    else
    {
        //QMessageBox::warning(this, "Login", "Invalid Username or Password");
        ui->statusBar->showMessage("Invalid Username or Password", 3000);
    }
}
