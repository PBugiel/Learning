#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // void about
    // void aboutQt
    // StandardButton critical
    // StandardButton information
    // StandardButton question
    // StandardButton warning

    //QMessageBox::critical(this, "So Mote It Be", "Totally critical spell");
    QMessageBox::StandardButton ret = QMessageBox::question(this, "So Mote It Be", "Would you use magick to harm others?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    if (ret == QMessageBox::Yes)
    {
        qDebug() << "Bad magician";
        QMessageBox::critical(this, "Be Warned!", "Magick shall be used for good and good only!");
    }
    else if (ret == QMessageBox::No)
    {
        qDebug() << "Good magician";
        QMessageBox::information(this, "Good", "You are fit for a great sorcerer my firend.");
    }
    else
    {
        qDebug() << "Hesitant magician";
        QMessageBox::warning(this, "Hm...?", "So you hesitate? Please be wise in your decisions");
    }
    QApplication::quit();
}
