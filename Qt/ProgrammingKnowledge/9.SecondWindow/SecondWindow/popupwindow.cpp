#include "popupwindow.h"
#include "ui_popupwindow.h"

PopupWindow::PopupWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PopupWindow)
{
    ui->setupUi(this);
}

PopupWindow::~PopupWindow()
{
    delete ui;
}
