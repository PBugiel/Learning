# USAGE
# python train.py

from core.bbox_regressor import ObjectDetector
from core.custom_tensor_dataset import CustomTensorDataset
from core import config
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader
from torchvision import transforms
from torch.nn import CrossEntropyLoss
from torch.nn import MSELoss
from torch.optim import Adam
from torchvision.models import resnet50
from sklearn.model_selection import train_test_split
from imutils import paths
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import pickle
import torch
import time
import cv2
import os


def train():
    print("[INFO] Loading dataset...")
    data = []
    labels = []
    bboxes = []
    image_paths = []

    # loop over CSVs with annotations
    for csv_path in paths.list_files(config.ANNOTS_PATH, validExts=".csv"):
        rows = open(csv_path).read().strip().split("\n")

        # loop over individual annotations (rows)
        for row in rows:
            row = row.split(",")
            (filename, x0_px, y0_px, x1_px, y1_px, label) = row

            image_path = os.path.sep.join([config.IMAGES_PATH, label, filename])
            image = cv2.imread(image_path)
            (h, w) = image.shape[:2]

            # bbox coordinates relative to image dimensions
            x0 = float(x0_px)/w
            x1 = float(x1_px)/w
            y0 = float(y0_px)/h
            y1 = float(y1_px)/h

            # preprocess image
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (224, 224))

            # append to lists
            data.append(image)
            labels.append(label)
            bboxes.append((x0, y0, x1, y1))
            image_paths.append(image_path)

    # convert the data to NumPy arrays - for faster data processing
    data = np.array(data, dtype="float32")
    labels = np.array(labels)
    bboxes = np.array(bboxes, dtype="float32")
    image_paths = np.array(image_paths)

    # label encoding - normalize labels such that they contain only values between 0 and n_classes-1
    le = LabelEncoder()
    labels = le.fit_transform(labels)

    # split data into train (80%) and test (20%)
    split = train_test_split(data, labels, bboxes, image_paths, test_size=0.2, random_state=42)
    (train_images, test_images) = split[:2]
    (train_labels, test_labels) = split[2:4]
    (train_bboxes, test_bboxes) = split[4:6]
    (train_paths, test_paths) = split[6:]

    # convert Numpy arrays to PyTorch tensors
    (train_images, test_images) = torch.tensor(train_images), torch.tensor(test_images)
    (train_labels, test_labels) = torch.tensor(train_labels), torch.tensor(test_labels)
    (train_bboxes, test_bboxes) = torch.tensor(train_bboxes), torch.tensor(test_bboxes)

    # define normalization transforms
    transformss = transforms.Compose([
        transforms.ToPILImage(),
        transforms.ToTensor(),
        transforms.Normalize(mean=config.MEAN, std=config.STD)
    ])

    # produce PyTorch datasets
    train_dataset = CustomTensorDataset((train_images, train_labels, train_bboxes), transforms=transformss)
    test_dataset = CustomTensorDataset((test_images, test_labels, test_bboxes), transforms=transformss)
    print("[INFO] Total training samples: {}...".format(len(train_dataset)))
    print("[INFO] Total testing samples: {}...".format(len(test_dataset)))

    # calculate steps per epoch
    train_steps = len(train_dataset) // config.BATCH_SIZE
    val_steps = len(test_dataset) // config.BATCH_SIZE

    # create data loaders
    train_loader = DataLoader(train_dataset, batch_size=config.BATCH_SIZE, shuffle=True,
                              num_workers=os.cpu_count(), pin_memory=config.PIN_MEMORY)
    test_loader = DataLoader(test_dataset, batch_size=config.BATCH_SIZE,
                             num_workers=os.cpu_count(), pin_memory=config.PIN_MEMORY)

    # save testing images paths for use in later evaluation
    print("[INFO] Saving testing image paths...")
    with open(config.TEST_PATHS, "w") as file:
        file.write("\n".join(test_paths))

    # load the ResNet50 network
    resnet = resnet50(pretrained=True)

    # freeze all ResNet layers, so they won't be updated during training
    for param in resnet.parameters():
        param.requires_grad = False

    # create a custom object detector and flash it to the computation device
    object_detector = ObjectDetector(resnet, len(le.classes_))
    object_detector = object_detector.to(config.DEVICE)

    # define loss function
    class_loss_func = CrossEntropyLoss()
    bbox_loss_func = MSELoss()

    # init the optimizer, compile the model, and show the model summary
    opt = Adam(object_detector.parameters(), lr=config.INIT_LR)
    print(object_detector)

    # initialize a dictionary to store training history
    history = {"total_train_loss": [], "total_val_loss": [], "train_class_acc": [], "val_class_acc": []}

    # loop over epochs
    print("[INFO] Training the network...")
    start_time = time.time()
    for e in tqdm(range(config.NUM_EPOCHS)):
        # set the model in training mode
        object_detector.train()

        # initialize the total training and validation loss
        total_train_loss = 0
        total_val_loss = 0

        # initialize the number of correct predictions in the training and validation step
        train_correct = 0
        val_correct = 0

        # loop over the training set
        for (images, labels, bboxes) in train_loader:
            # send images to computation device
            (images, labels, bboxes) = (images.to(config.DEVICE), labels.to(config.DEVICE), bboxes.to(config.DEVICE))

            # perform a forward pas and calculate the training loss
            predictions = object_detector(images)
            bbox_loss = bbox_loss_func(predictions[0], bboxes)
            class_loss = class_loss_func(predictions[1], labels)
            total_loss = (config.BBOX * bbox_loss) + (config.LABELS * class_loss)

            # zero out the gradients, perform the backpropagation step, and update the weights
            opt.zero_grad()
            total_loss.backward()
            opt.step()

            # add the loss to the total training loss so far and calculate the number od correct predictions
            total_train_loss += total_loss
            train_correct += (predictions[1].argmax(1) == labels).type(torch.float).sum().item()

        # turn off autograd
        with torch.no_grad():
            # set the model in evaluation mode
            object_detector.eval()

            # loop over the validation set
            for (images, labels, bboxes) in test_loader:
                # send input to the device
                (images, labels, bboxes) = (images.to(config.DEVICE), labels.to(config.DEVICE), bboxes.to(config.DEVICE))

                # make predictions and calculate the validation loss
                predictions = object_detector(images)
                bbox_loss = bbox_loss_func(predictions[0], bboxes)
                class_loss = class_loss_func(predictions[1], labels)
                total_loss = (config.BBOX * bbox_loss) + (config.LABELS * class_loss)
                total_val_loss += total_loss

                val_correct += (predictions[1].argmax(1) == labels).type(torch.float).sum().item()

        # calculate the average training and validation loss
        avg_train_loss = total_train_loss / train_steps
        avg_val_loss = total_val_loss / val_steps

        # calculate the training and validation accuracy
        train_correct = train_correct / len(train_dataset)
        val_correct = val_correct / len(test_dataset)

        # update the training history
        history["total_train_loss"].append(avg_train_loss.cpu().detach().numpy())
        history["train_class_acc"].append(train_correct)
        history["total_val_loss"].append(avg_val_loss.cpu().detach().numpy())
        history["val_class_acc"].append(val_correct)

        # print the model training and validation information
        print("[INFO] EPOCH: {}/{}".format(e+1, config.NUM_EPOCHS))
        print("Train loss: {:.6f}, Train accuracy: {:.4f}.".format(avg_train_loss, train_correct))
        print("Val loss: {:.6f}, Val accuracy: {:.4f}.".format(avg_val_loss, val_correct))

    end_time = time.time()
    print("[INFO] Total time taken to train the model: {:2f}s".format(end_time-start_time))

    # serialize the model to disk
    print("[INFO] Saving object detector model to disk: {}".format(config.MODEL_PATH))
    torch.save(object_detector, config.MODEL_PATH)

    # serialize the label encoder to disk
    print("[INFO] saving label encoder: {}".format(config.LE_PATH))
    with open(config.LE_PATH, "wb") as f:
        f.write(pickle.dumps(le))

    # plot the training loss and accuracy
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(history["total_train_loss"], label="total_train_loss")
    plt.plot(history["total_val_loss"], label="total_val_loss")
    plt.plot(history["train_class_acc"], label="train_class_acc")
    plt.plot(history["val_class_acc"], label="val_class_acc")
    plt.title("Total Training Loss and Classification Accuracy on Dataset")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss Accuracy")
    plt.legend(loc="lower left")
    plt.show()

    # save the training plot
    plot_path = os.path.sep.join(([config.PLOTS_PATH, "training.png"]))
    plt.savefig(plot_path)


if __name__ == "__main__":
    train()
