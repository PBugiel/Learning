import torch
import torch.nn as nn
import torch.nn.functional as ff
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
from torchvision.utils import make_grid

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import time
import pickle


# Load data
with open("train_and_test_stats.pkl", "rb") as file:
    (train_losses, test_losses, train_corrects, test_corrects) = pickle.load(file)

# Graph plot at each epoch
train_losses = [tl.item() for tl in train_losses]  # Convert tensor to Python list
plt.plot(train_losses, label="Training Loss")
plt.plot(test_losses, label="Validation Loss")
plt.title("Loss at Epoch")
plt.legend()
plt.show()

# Graph the accuracy at the end of each epoch
plt.plot([t/600 for t in train_corrects], label="Training accuracy")
plt.plot([t/100 for t in test_corrects], label="Validation accuracy")
plt.title("Accuracy at the end of each epoch")
plt.legend()
plt.show()
