import torch
import torch.nn as nn
import torch.nn.functional as ff
from torchvision import datasets, transforms


# Convert MNIST image files into a 4-dimensional tensor
mnist_transform = transforms.ToTensor()

# Train data
train_data = datasets.MNIST(root="./mnist_data", train=True, download=True, transform=mnist_transform)
print(train_data)
# Test data
test_data = datasets.MNIST(root="./mnist_data", train=False, download=True, transform=mnist_transform)

# Define CNN model - just an example
conv1 = nn.Conv2d(1, 6, 3, 1)
conv2 = nn.Conv2d(6, 16, 3, 1)

# Grab one MNIST image
x_train = torch.Tensor
for i, (x_train, y_train) in enumerate(train_data):
    break

print(x_train.shape, "\tX out of the box")
x = x_train.reshape(1, 1, 28, 28)
print(x.shape, "\tX after reshape")

# Perform first convolution
x = ff.relu(conv1(x))
print(x.shape, "\tX after 1st convolution")

# Pooling layer
x = ff.max_pool2d(x, 2, 2)  # kernel 2, stride 2
print(x.shape, "\tX after 1st pooling")

# Second convolution
x = ff.relu(conv2(x))
print(x.shape, "\tX after 2nd convolution")

# Another pooling
x = ff.max_pool2d(x, 2, 2)
print(x.shape, "\tX after 2nd pooling")