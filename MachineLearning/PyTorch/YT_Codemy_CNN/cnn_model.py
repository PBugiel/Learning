import torch.nn as nn
import torch.nn.functional as ff


class CnnModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 6, 3, 1)
        self.conv2 = nn.Conv2d(6, 16, 3, 1)
        self.fc1 = nn.Linear(5 * 5 * 16, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # input images are of size 28x28
        x = ff.relu(self.conv1(x))  # => 6x26x26
        x = ff.max_pool2d(x, 2, 2)  # => 6x13x13
        x = ff.relu(self.conv2(x))  # => 16x11x11
        x = ff.max_pool2d(x, 2, 2)  # => 16x5x5

        # Reshape to flatten the data
        x = x.reshape(-1, 5 * 5 * 16)  # -1 so we can vary the batch size
        # Fully connected layer
        x = ff.relu(self.fc1(x))
        x = ff.relu(self.fc2(x))
        x = self.fc3(x)
        return ff.log_softmax(x, dim=1)