import matplotlib.pyplot as plt
import torch
from cnn_model import CnnModel
from torchvision import datasets, transforms


# Load data and model
mnist_transform = transforms.ToTensor()
test_data = datasets.MNIST(root="./mnist_data", train=False, download=True, transform=mnist_transform)
model = CnnModel()
model.load_state_dict(torch.load("SavedCnnModel.pt", weights_only=True))

# Loop over images and make predictions
img_idx = 0
increment = 900
while img_idx < len(test_data):
    img_data = test_data[img_idx][0]
    # unsqueeze(0) adds new axis at the beginning, here == .reshape(1,1,28,28) as the input is of shape (1,28,28)
    prediction = model(img_data.unsqueeze(0))
    img_data = torch.flatten(img_data, end_dim=1)  # flattens the first dimension: (1,28,28) => (28,28)
    plt.imshow(img_data, cmap='gray')
    plt.title(f"Ground truth: {test_data[img_idx][1]}, prediction: {prediction.argmax().item()}")
    plt.pause(1.5)
    img_idx += increment
