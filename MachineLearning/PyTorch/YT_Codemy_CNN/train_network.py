import pickle

import torch
import torch.nn as nn
import torch.nn.functional as ff
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
from torchvision.utils import make_grid

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import time

from cnn_model import CnnModel

torch.manual_seed(41)

# Create instance of the CnnModel
model = CnnModel()
print(model)

# Loss function optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

# Data Loaders
mnist_transform = transforms.ToTensor()  # Convert MNIST image files into a 4-dimensional tensor
train_data = datasets.MNIST(root="./mnist_data", train=True, download=True, transform=mnist_transform)
test_data = datasets.MNIST(root="./mnist_data", train=False, download=True, transform=mnist_transform)
train_loader = DataLoader(train_data, batch_size=10, shuffle=True)
test_loader = DataLoader(test_data, batch_size=10, shuffle=False)

# Training
start_time = time.time()

epochs = 5
train_losses = []
test_losses = []
train_corrects = []
test_corrects = []

# Loop over epochs
for i in range(epochs):
    train_correct = 0
    test_correct = 0
    # Train
    for batch, (X_train, Y_train) in enumerate(train_loader):
        batch += 1  # start at 1
        y_pred = model(X_train)  # get predicted values
        loss = criterion(y_pred, Y_train)  # cross entropy loss
        predicted = torch.max(y_pred.data, 1)[1]  # add up number of correct prediction; indexed off the first point...?
        batch_corr = (predicted == Y_train).sum()  # how many are correct in the batch
        train_correct += batch_corr

        # Update parameters
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # print out results
        if batch % 600 == 0:
            print(f"Epoch: {i}, batch: {batch}, loss: {loss.item()}, batch_correct: {batch_corr}")

    train_losses.append(loss)
    train_corrects.append(train_correct)

    # Test
    with torch.no_grad():
        for batch, (X_test, Y_test) in enumerate(test_loader):
            y_val = model(X_test)
            predicted = torch.max(y_val.data, 1)[1]
            test_correct += (predicted == Y_test).sum()

            # TODO indent?
            loss = criterion(y_val, Y_test)
            test_losses.append(loss)
            test_corrects.append(test_correct)

total_time = time.time() - start_time
print(f"Training took {total_time / 60} minutes")

# Save the model
torch.save(model.state_dict(), "SavedCnnModel.pt")

# Save losses
with open("train_and_test_stats.pkl", "wb") as file:
    pickle.dump((train_losses, test_losses, train_corrects, test_corrects), file)

