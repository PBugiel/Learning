import torch
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
from cnn_model import CnnModel


mnist_transform = transforms.ToTensor()  # Convert MNIST image files into a 4-dimensional tensor
test_data = datasets.MNIST(root="./mnist_data", train=False, download=True, transform=mnist_transform)
model = CnnModel()
model.load_state_dict(torch.load("SavedCnnModel.pt", weights_only=True))
# Test everything
test_loader_all = DataLoader(test_data, batch_size=len(test_data), shuffle=False)
with torch.no_grad():
    correct = 0
    for X_test, Y_test in test_loader_all:
        y_val = model(X_test)
        predicted = torch.max(y_val, 1)[1]  # Torch.max returns max value and index of this value - hence [1]
        correct += (predicted == Y_test).sum()
    print(f"Accuracy on whole test est: {correct.item() / len(test_data) * 100}")
